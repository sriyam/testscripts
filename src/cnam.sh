echo "TEST1 - 200 Ok Status Unavailable" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16303060003"&called_party="16307551540"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST2 - 200 OK" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16304147970"&called_party="16307551540"&api_key="vdfdweresdsd9a0sdree"' 
exit
echo
echo "TEST3 - 200 Ok Status Unavailable" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="1AB44559111"&called_party="16302101979"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST4 - 200 Ok Status Unavailable" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="198048580"&called_party="16307551540"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST5 - 200 Ok Status=Unavailable" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="1121212121211"&called_party="16302101979"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST6 - 200 Ok Status=Unavailable" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="31212121213"&called_party="16304147970"&api_key="vdfdweresdsd9a0sdree"'
echo
echo "TEST7 - 200 Ok CNAM Not Available" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16303060003"&called_party="16304147970"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST8 - 200 OK DanIsTheCNAM" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16304147970"&called_party="16307551540"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST09 - 200 null name"
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16309992222"&called_party="16307551540"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST10 - 404 code 10" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="11212121212"&called_party="16308158552"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST11 -404 code 20 " 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="11212121212"&called_party="16307551543"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST12 - 404 code 30" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="11212121213"&called_party="16307551542"&api_key="vdfdweresdsd9a0sdree"'
echo
echo "TEST13 - 404 code 40" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="11212121212"&called_party="16304147973"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST14 - 200 code 50" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="11212121212"&called_party="16825825747"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST15 - 405 method not allowed" 
curl -i -X POST -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16303060003"&called_party="16304147970"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST16 - 400 Bad Request" 
curl -i -H "Content-Type: application/json" -H "Accept: application/xml" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16303060003"&called_party="16304147970"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST17 - 400 Bad Request" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/dantest/v1/identity?calling_party="16303060003"&called_party="16304147970"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST18 - 403 forbidden" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/badname/ecid/v1/identity?calling_party="16303060003"&called_party="16304147970"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST19 -200 Scam Likely"
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="11212121212"&called_party="16307551543"&api_key="vdfdweresdsd9a0sdree"' 
echo
echo "TEST100 - 200 OK" 
curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16303060003"&called_party="16308158552"&api_key="vdfdweresdsd9a0sdree"' 
echo
#echo "TEST101
#curl -i -H "Content-Type: application/json" -H "Accept: application/json" 'http://172.22.43.39:8081/firstorion/ecid/v1/identity?calling_party="16303060003"&called_party="16304147970"&api_key="vdfdweresdsd9a0sdree"' 
#echo
