IP1=172.22.23.173:5060
IP2=208.54.8.113:5060
IP3=172.22.23.173:5060
if
  test "${1}" = ""
then
  TEST=""
else
  case ${1} in
    INFORM*)
      TEST=`grep ^${1}: inform_tests.data`
      ;;
    *)
      TEST=`grep ^${1}: fpos_tests.data`
      ;;
  esac
fi
if
  test "$TEST" = ""
then
  echo "Usage: gen_test.sh scam001 [noPAI] - to generate a new INVITE template"
  echo "Usage: sendINV.sh scam001 - to send an existing INVITE template in ./invites"
  echo "valid test cases are: "
  tail -n+2 *.data | cut -f1 -d":" 
  exit
fi
TTT=`echo $TEST | cut -f1 -d":"`
PAI=`echo $TEST | cut -f2 -d":"`
A=`echo $TEST | cut -f3 -d":"`
RURI=`echo $TEST | cut -f4 -d":"`
B=`echo $TEST | cut -f5 -d":"`
PI=`echo $TEST | cut -f6 -d":"`
if
  test "$A" = ""
then
  A=$PAI
fi
if
  test "$A" = "anonymous"
then
  IP3="anonymous.com"
fi
if
  test "$PAI" != "" -a "$2" != ""
then
  if
    test "$PAI" = "anonymous"
  then
    PPPPP="P-Asserted-Identity:  <sip:anonymous@anonymous.com;user=phone>"
  else
    PPPPP="P-Asserted-Identity:  <sip:${PAI}@${IP1};user=phone>"
  fi
else
  PPPPP=PPPPP
fi
if
  test "$B" = ""
then
  B=$RURI
fi
if
  test "$PI" != ""
then
  PI="Privacy: id"
else
  PI=PIPI
fi
echo A:$A:RURI:$RURI:B:$B
echo PAI:$PPPPP
echo PI:$PI
cat $HOME/invites/INVITE.templ |\
  sed -e "s/TEST/${TTT}/g" |\
  sed -e "s/RURI/${RURI}/g" |\
  sed -e "s/AAAAA/${A}/g" |\
  sed -e "s/BBBBB/${B}/g" |\
  sed -e "s/PIPI/${PI}/g" |\
  sed -e "s/IP1/${IP1}/g" |\
  sed -e "s/IP2/${IP2}/g" |\
  sed -e "s/IP3/${IP3}/g" |\
  sed -e "s/PPPPP/${PPPPP}/g" |\
  sed -e "s/RURI/${PAI}/g" |\
  grep -v PPPPP |\
  grep -v PIPI > $HOME/invites/INVITE.$TTT
cat $HOME/invites/INVITE.$TTT
./sendINV.sh $TTT
