TEST=${1}
P1=$2
VERBOSE=${3}
VERBOSE=v
if
  test "${TEST}" = ""
then
  echo "need a test#"
  exit
fi
if
  test "$VERBOSE" = "v"
then
  #$HOME/gen2.sh $TEST $P1
  $HOME/nxtgen.sh $TEST $P1
else
  #$HOME/gen2.sh $TEST $P1 1>/dev/null 
  $HOME/nxtgen.sh $TEST $P1 1>/dev/null 
fi
#$HOME/check_test.sh $TEST
echo $TEST
$HOME/diff_log.sh $TEST ${VERBOSE}
