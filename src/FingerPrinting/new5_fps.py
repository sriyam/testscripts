#!/usr/bin/env python

import socket
import argparse
import sys
import re
import json
#import time
import datetime
import base64

HOST = '0.0.0.0'
PORT = 5060
VERBOSE = False

class SIPInvite:

   def __init__(self):
      self._from = None
      self._to = None
      self._toPhone = None
      self._fromPhone = None
      self._requestURIPhone = None
      self._callID = None
      self._cSeq = None
      self._pAssertedID = None
      self._pAssertedPhone = None
      self._via = []
      self._isOptions = None


def parseArgs():

   parser = argparse.ArgumentParser(description="Test FPS")

   parser.add_argument("-v", "--verbose", action="store_true",
                        help="(verbose)")

   parser.add_argument("-p", "--port", action="store", dest="port", 
                        help="(UDP listen port - default 5060)")

   parser.add_argument("-i", "--host", action="store", dest="host",
                        help="(Host IP - default 0.0.0.0)")

   parser.add_argument("-c", "--config", action="store", dest="config",
                        help="Name of config file")

   args = parser.parse_args()

   if args.verbose == True:
      global VERBOSE
      VERBOSE = True

   if args.port is not None:
      global PORT 
      PORT = (int)(args.port)

   if args.host is not None:
      global HOST 
      HOST = args.host

   return(args)

def getHeaderValue(header):
   start = header.index(':')
   #account for the space after :
   return header[(start+2):]

def extractPhone(value):
   #advance to next ':' (could be 'sip:' or 'tel:')
   start = value.index(':')
   #advance over leading '+'
   if value[start+1] == '+' and (start + 1) < len(value):
      start+=1

   searchstring = value[(start+1):]
   #find non-digit terminating character
   end = re.search(r'[^1234567890]', searchstring).start()
   phone = searchstring[:(end)]
   return phone

def parseHeader(header, invite):

   if header.upper().startswith('INVITE'):
      i = header.index(' ')
      requestURI = header[(i+1):]
      invite._requestURIPhone = extractPhone(requestURI)

   #both regular header and short header
   elif header.upper().startswith('FROM:') or header.upper().startswith('F:'):
      value = getHeaderValue(header)
      invite._fromHeader = value
      #start = value.index('<')
      #invite._fromPhone = extractPhone(value[(start+1):])
      invite._fromPhone = extractPhone(value)

   elif header.upper().startswith('TO:') or header.upper().startswith('T:'):
      value = getHeaderValue(header)
      invite._toHeader = value
      #start = value.find('<')
      #if start >= 0:
      #   invite._toPhone = extractPhone(value[(start+1):])
      #else:
      #   invite._toPhone = value
      invite._toPhone = extractPhone(value)

   elif header.upper().startswith('CALL-ID:') or header.upper().startswith('I:'):
      value = getHeaderValue(header)
      invite._callID = value

   elif header.upper().startswith('CSEQ:'):
      value = getHeaderValue(header)
      invite._cSeq = value

   elif header.upper().startswith('P-ASSERTED-IDENTITY:'):
      value = getHeaderValue(header)
      invite._pAssertedID = value
      #try:
      #    start = value.index('<')
      #    invite._pAssertedPhone = extractPhone(value[(start+1):])
      #except:
      #    invite._pAssertedPhone = extractPhone(value)
      invite._pAssertedPhone = extractPhone(value)

   elif header.upper().startswith('VIA:') or header.upper().startswith('V:'):
      value = getHeaderValue(header)
      invite._via.append(value)

def parseInvite(message):

   invite = SIPInvite()
   isInvite = False
   datetime_object = datetime.datetime.now()

   lines = message.splitlines()

   if lines[0].startswith('INVITE'):
      invite._isOptions = False

      for line in lines:
         parseHeader(line, invite)

      if (VERBOSE):
         print("INVITE Parsed fields ------------------------------------");
         print(datetime_object) 
         print("from           = [%s]" % invite._fromHeader);
         print("Call-ID        = [%s]" % invite._callID);
         print("fromPhone      = [%s]" % invite._fromPhone);
         print("pAssertedPhone = [%s]" % invite._pAssertedPhone);
         print("URIPhone       = [%s]" % invite._requestURIPhone);
         print("to             = [%s]" % invite._toHeader);
         print("toPhone        = [%s]" % invite._toPhone);
         print("via            = [%s]\n" % invite._via);

      return invite
   elif lines[0].startswith('OPTIONS'):

      invite._isOptions = True
      for line in lines:
         parseHeader(line, invite)

      if (VERBOSE):
         print(datetime_object) 
         print("OPTIONS Response --------------------------------");
         #print("From           = [%s]" % invite._fromHeader);
         #print("To             = [%s]" % invite._toHeader);
         #print("Call-ID        = [%s]" % invite._callID);
         #print("CSeq           = [%s]" % invite._cSeq);
         #print("Via            = [%s]\n" % invite._via); 

      return invite  

   else:
      print("Not a INVITE or OPTIONS!")
      return None

def sendReply(serverSocket, invite, returnAddress, config):
   # test_resp = ("SIP/2.0 200 OK\r\n"
   #              "Via: SIP/2.0/UDP 172.31.20.184:41504;branch=z9hG4bK-14421-125-0\r\n"
   #              "From: sipp <sip:+1083463984@172.31.20.184:41504>;tag=14421SIPpTag00125\r\n"
   #              "To: service <sip:+1394078447@127.0.0.1:15060>\r\n"
   #              "Call-ID: 9f194ac5-3d96-4870-b740-f1c9844c0d39\r\n"
   #              "CSeq: 1 INVITE\r\n"
   #              "Content-Length: 134\r\n\r\n"
   #              "{\"PAIexistInINVITE\":false,\"bucketScores\":[[0,5],[15,5]],\"cdpn\":1394078447,\"cgpn\":1083463984,\"influenceAlgos\":[\"0\",\"1688849860263937\"]")
   if(invite._isOptions == True):
      response =  "SIP/2.0 200 OK\r\n"
      for line in invite._via:
         response += "Via: " + line + "\r\n"
      response += "From: " + invite._fromHeader + "\r\n"
      response += "To: " + invite._toHeader + "\r\n"
      response += "Call-ID: " + invite._callID + "\r\n"
      response += "CSeq: " + invite._cSeq + "\r\n"
      response += "Allow: INVITE, OPTIONS" + "\r\n"
      response += "Content-Length: 0" + "\r\n\r\n"

   else:
      PAIexistInINVITE = False  
      cgpn = ""
      if invite._pAssertedPhone is not None:
         if type(invite._pAssertedPhone) == int:
            print("got integer PAI!")
            cgpn = int(invite._pAssertedPhone)
         else:
            print("got non-integer PAI! check From")
            if type(invite._fromPhone) == int:
               print("got integer From!")
               cgpn = int(invite._fromPhone)
            else:
               #cgpn = invite._fromPhone
               print("use non-integer PAI!")
               cgpn = invite._pAssertedPhone
      else:
            print("no PAI! check From")
            if type(invite._fromPhone) == int:
               print("got integer From!")
               cgpn = int(invite._fromPhone)
            else:
               print("use non-integer From!")
               cgpn = invite._fromPhone
   
      print("requestURI: %s" % invite._requestURIPhone)
      if(invite._requestURIPhone is not None):
      	if invite._requestURIPhone.isdigit == False:
         	cdpn = ""
        else:
      		cdpn = invite._requestURIPhone
      else:
         cdpn = ""
   
      if invite._pAssertedID is not None and len(invite._pAssertedID) > 0:
         PAIexistInINVITE = True
      bucketScores = getScore(cgpn,cdpn,config)
      #bucketScores = getScore(invite._fromPhone,config)
   
      influenceAlgos = ["charles_v2", "charles_with_lerg"]
   
      #time.sleep(2)
   
      data = {"cgpn":cgpn, "cdpn":cdpn, "PAIexistInINVITE":PAIexistInINVITE, 
              "bucketScores":bucketScores, "influenceAlgos":influenceAlgos}
   
      json_data = json.dumps(data)
   
      response =  "SIP/2.0 200 OK\r\n"
      for line in invite._via:
         response += "Via: " + line + "\r\n"
      response += "From: " + invite._fromHeader + "\r\n"
      response += "To: " + invite._toHeader + "\r\n"
      response += "Call-ID: " + invite._callID + "\r\n"
      response += "CSeq: " + invite._cSeq + "\r\n"
      response += "Content-Length: " + (str)(len(json_data)) + "\r\n\r\n"
      response += json_data

   if (VERBOSE):
      if(invite._isOptions == False):
        print("sending INVITE response\n%s\n" % response)
      #else:
      #  print("sending OPTIONS response\n%s\n" % response)

   serverSocket.sendto(response, returnAddress)

def getScore(fromPhone, toPhone, config):
   #get the score associated with the number and if it doesn't exist set score to 1
   score = config['numbers'].get(str(fromPhone), '0')
   print("toPhone: %s; fromPhone: %s; score: %s %s" % (toPhone, fromPhone, score, score == "0"))
   if toPhone == 14693216518 or toPhone == 19804858091:
      if (score == "0"):
         bucketScores = [[13, 1]]
      else:
         bucketScores = [[14, 1]]
   else:
      catid = int(score) - 255
      if catid > 0:
      	bucketScores = [[catid, score],[0, catid],[15, catid]]
      else:
      	bucketScores = [[0, score],[15, score]]
   return bucketScores

def readConfigFile(configFile):
      with open(configFile) as json_data_file:
         data = json.load(json_data_file)
         return data

def main(argc, argv):

    args = parseArgs()
    config = readConfigFile(args.config)

    #udp socket
    serverSocket = socket.socket(
                socket.AF_INET, socket.SOCK_DGRAM)
    print("Listening on HOST: %s, PORT %d" % (HOST, PORT))
    serverSocket.bind((HOST, PORT))

    while True:
        message, returnAddress = serverSocket.recvfrom(4096)
        if len(message) > 0:
            invite = parseInvite(message)
            if invite is not None:
               sendReply(serverSocket,invite,returnAddress,config)

if __name__ == "__main__":
   main(len(sys.argv), sys.argv)
