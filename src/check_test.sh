DIR=$HOME
VM=18005551211
HIH=0
HDRONLY=N
CHKVS=0
CR=0
# LABS
FOAWS=172.22.43.180
#FOAWS=172.22.23.185
#FOAWS=172.22.43.180
case $FOAWS in
  "172.22.43.180")
      SRCIP=172.22.23.173
      SRCPORT=9090
      DSTIP=$FOAWS
      DSTPORT=7979
      ;;
  "172.22.23.185")
      SRCIP=172.22.23.173
      SRCPORT=8085
      DSTIP=$FOAWS
      DSTPORT=6565
      ;;
  "172.22.43.39")
      SRCIP=172.22.23.173
      SRCPORT=9191
      DSTIP=$FOAWS
      DSTPORT=9090
      ;;
  *)
      echo "Invalid lab config"
      exit
      ;;
esac
TEST=`grep ^${1}: tests.data`
if
  test "$TEST" = ""
then
  echo "Usage: check_test.sh scam001 [TYPE] - to send an INVITE to Delegator"
  echo "valid test cases are: "
  #tail -n+2 *.data | cut -f1 -d":" 
  exit
fi
TESTNUM=`echo $TEST | cut -f1 -d":"`
C4=`echo $TESTNUM | cut -c1-4`
LOGNAME="./logs/${TESTNUM}.log"
RESPF="/tmp/$$.${TESTNUM}_resp"
if
  test ! -f $LOGNAME
then
  echo "1:$TESTNUM:no output file to check"
  exit
fi
AS=""
if
  test "$C4" = "stir" -a "`echo $TESTNUM | grep '-'`" = ""
then
  AS="-as"
  if
    test "$2" != "ASONLY"
  then
    CHKVS=1
  fi
fi
A=`echo $TEST | cut -f2 -d":"`
B=`echo $TEST | cut -f3 -d":"`
if
  test "$C4" = "rest" -o "$C4" = "mpnb"
then
  # REST PARMS
  RC=`echo $TEST | cut -f4 -d":"`
  URI=`echo $TEST | cut -f5 -d":"`
  RSPC=`echo $TEST | cut -f6 -d":"`
  ACT=`echo $TEST | cut -f7 -d":"`
  REASC=`echo $TEST | cut -f8 -d":"`
  FWDN=`echo $TEST | cut -f9 -d":"`
  TAG=`echo $TEST | cut -f10 -d":"`
  PROT=$C4
else
  PAI=`echo $TEST | cut -f4 -d":"`
  PI=`echo $TEST | cut -f5 -d":"`
  RSPC=`echo $TEST | cut -f6 -d":"`
  ADDL=`echo $TEST | cut -f7 -d":"`
  MODL=`echo $TEST | cut -f8 -d":"`
  DELL=`echo $TEST | cut -f9 -d":"`
  TAG=`echo $TEST | cut -f10 -d":"`
  PROT=sip
fi
#if 
#  [ "$PAI" -eq "$PAI" ] 2>/dev/null
#then
#  if 
#    test ${#PAI} -gt 7
#  then
#    A=$PAI
#  fi
#fi
case $RSPC in
  200)
      RSPH=" OK"
      EC=0
      ;;
  302)
      RSPH=" Moved Temporarily"
      CC=1
      CR=0
      ;;
  400)
      RSPH=" Bad Request"
      case $TESTNUM in
	rest016 | rest017 | rest018 | rest019)
           HDRONLY=Y
           ;;
	mpnb016 | mpnb017 | mpnb018 | mpnb019)
           HDRONLY=Y
           ;;
        *)
           ;;
      esac
      EC=0
      ;;
  403)
      RSPH=" Forbidden"
      HDRONLY=Y
      EC=0
      ;;
  404)
      RSPH=" Not Found"
      EC=1
      ;;
  405)
      RSPH=" Method Not Allowed"
      HDRONLY=Y
      EC=0
      ;;
  487)
      RSPC=487
      RSPH=" Request Terminated"
      CC=0
      CR=1
      ;;
  *)
      RSPH=" Unknown Response"
      echo "1:$TESTNUM${AS}:Unknown response code $RSPC"
      FLG=1
      ;;
esac
STRESPL=`grep -n -m1 "${RSPC}${RSPH}" ${LOGNAME} | cut -f1 -d":"`
if
  test "${STRESPL}" = "" 
then
  echo "1:$TESTNUM${AS}:Cannot find $RSPC response"
  exit
fi
if
  test "$HDRONLY" != "Y"
then
case $PROT in
  # SIP ************************************************
  sip)
ENDRESPL=`grep -n -m1 "Content-Length: 0" ${LOGNAME} | cut -f1 -d":"`
if
  test "${ENDRESPL}" = ""
then
  echo "1:$TESTNUM${AS}:Cannot find end line for $RSPC response"
  exit
else
  tail -n +${STRESPL} $LOGNAME | head -n $(($ENDRESPL - $STRESPL + 1)) > $RESPF
fi
if
  test "$CC" = "1"
then
  #CHECK CONTACT
  HIH=`echo $ADDL | grep History-Info | wc -l`
  if
    test $HIH = "1"
  then
    CONTACT="Contact: <sip:+${VM}@t-mobile.com;user=phone;enum=yes>"
  else
    case $C4 in
      emer)
        CONTACT="Contact: <sip:+${B}@${DSTIP}:${DSTPORT};user=phone"
        ;;
      stir )
        if
          test "`echo $TESTNUM | cut -f2 -d"-"`" = "vs"
        then
          CONTACT="Contact: <sip:+${B}@${DSTIP}:${DSTPORT};user=phone"
        else
          AS_DSTPORT=`grep -m1 AS_DSTPORT ./call_delegator.sh | cut -f2 -d"="`
          CONTACT="Contact: <sip:+${B}@${DSTIP}:${AS_DSTPORT};user=phone"
        fi
        ;;
      *)
        CONTACT="Contact: <sip:+${B}@${DSTIP}:${DSTPORT};user=phone;enum=yes>"
        ;;
    esac
  fi
  #echo $CONTACT
  #grep Contact $RESPF
  FND=`grep "$CONTACT" $RESPF`
  if
    test "${FND}" = ""
  then
    echo "1:$TESTNUM${AS}:Contact Header does not match expectations"
    FLG=1
  fi
fi
#CHECK PSTI
PSTI="P-STI-Header-Info:"
for X in `echo add:$ADDL modify:$MODL delete:$DELL`
do
  TYPE=`echo $X | cut -f1 -d":"`
  LIST=`echo $X | cut -f2- -d":"`
  if
    test "$LIST" != "NA" 
  then
    while
      test "$LIST" != ""
    do
      CHGHDR=`echo $LIST | cut -f1 -d","`
      case $CHGHDR in
          in-resp*)
             TYPE="add-in-resp"
             ReasonCode=`echo ${CHGHDR} | cut -c14-`
             CHGHDR="Reason"
             CR=2
             ;;
          Reason*)
             ReasonCode=`echo ${CHGHDR} | cut -c7-`
             CHGHDR="Reason"
             CR=1
             ;;
          *)
             ;;
      esac
      HDR=`grep "${PSTI} ${TYPE}=" $RESPF`
      case "$HDR" in
        *${CHGHDR}*)
            #echo we have the right PSTI headers
            ;;
        *)
            echo "1:$TESTNUM${AS}:PSTI Header $TYPE does not match expectations"
            FLG=1
            ;;
      esac
      LIST=`echo $LIST | cut -s -f2- -d","`
    done
  else
    #echo $HDR
    #CHECK FOR EXTRA PSTI HEADERS
    HDR=`grep "${PSTI} ${TYPE}=" $RESPF`
    if
      test "${HDR}" != ""
    then
      #PHDR=`echo ${HDR} | sed -e "s/\"/\\\"/g"`
      echo "1:${TESTNUM}${AS}:Extra PSTI Header in response: ${HDR}"
      FLG=1
    fi
  fi
done
#CHECK TAG
if
  test "$TAG" != "" -a "$CC" = "1"
then
  VERSTAT=""
  VEREXIST=`echo $TAG | cut -s -f2 -d";"`
  if
    test "$VEREXIST" != ""
  then
    VERSTAT="verstat=`echo $TAG | cut -s -f2 -d";"`"
  fi
  STAMP=`echo $TAG | cut -f1 -d";"`
  if
    test "$C4" = "stir"
  then
    if
      test "$AS" = "-as"
    then
      # CHECK ATTESTATON IND
      HDR="P-Attestation-Indicator: ${STAMP}"
      CHKHDR=`grep "${HDR}" $RESPF | tr -d '\r'`
      if
        test "${CHKHDR}" = ""
      then
        echo "1:$TESTNUM${AS}:value $HDR does not appear correctly in result"
        FLG=1
      fi
    else
      CHKHDR=`grep "From" $RESPF | tr -d '\r' | grep $VERSTAT | grep "$STAMP"`
      if
        test "${CHKHDR}" = ""
      then
        echo "1:$TESTNUM${AS}:value $STAMP does not appear correctly in From"
        FLG=1
      fi
      if 
        test "${PAI}" != ""
      then
        CHKHDR=`grep "P-Asserted-Identity" $RESPF | tr -d '\r' | grep $VERSTAT | grep "$STAMP"`
        if
          test "${CHKHDR}" = ""
        then
          echo "1:$TESTNUM${AS}:value $STAMP does not appear correctly in P-Asserted-Identity"
          FLG=1
        fi
      fi
    fi
  else
  for HDR in `echo From P-Asserted-Identity`
  do
    if
      test "$PI" != "" -a "$STAMP" = "Scam Likely"
    then
      if 
        test "${VERSTAT}" != ""
      then
        TAGHDR="${HDR}: \"${STAMP}\" <sip:+0000000000@${SRCIP}:${SRCPORT};user=phone;$VERSTAT>"
      else
        TAGHDR="${HDR}: \"${STAMP}\" <sip:+0000000000@${SRCIP}:${SRCPORT};user=phone>"
      fi
    else
      if
        test "$HDR" = "From"
      then
        if 
          test "${STAMP}" = ""
        then
          STAMP="test artifacts"
        fi
        if 
          test "${VERSTAT}" != ""
        then
          TAGHDR="${HDR}: \"${STAMP}\" <sip:+$A@${SRCIP}:${SRCPORT};user=phone;$VERSTAT>"
	else
          TAGHDR="${HDR}: \"${STAMP}\" <sip:+$A@${SRCIP}:${SRCPORT};user=phone>"
	fi
      else
        if 
          test "$PAI" = "" -o "$PAI" = "PAI" -o "$PAI" = "2PAI" -o "$PI" = "TO"
        then
          PAI=$A
        fi
        if 
          test "${VERSTAT}" != ""
        then
          TAGHDR="${HDR}: \"${STAMP}\" <sip:+$PAI@${SRCIP}:${SRCPORT};user=phone;$VERSTAT>"
	else
          TAGHDR="${HDR}: \"${STAMP}\" <sip:+$PAI@${SRCIP}:${SRCPORT};user=phone>"
        fi
      fi
    fi
    #CHECK EXPECTED CHANGED HEADERS
    CHKHDR=`grep "${HDR}:" $RESPF | grep sip | tr -d '\r'`
    #echo $TAGHDR
    #echo $CHKHDR
    case "$CHKHDR" in
      ${TAGHDR})
           ;;
      ${TAGHDR}\;tag=*)
           if
             test "$HDR" != "From"
           then
	     # PAI is exact match - From can have a tag at end
             echo "1:$TESTNUM${AS}:TAG $TAG does not appear correctly in $HDR"
             FLG=1
           fi
           ;;
      *)
           echo "1:$TESTNUM${AS}:TAG $TAG does not appear correctly in $HDR"
           FLG=1
           ;;
    esac
  done
  fi
fi
#set +x
#CHECK HIH
if
  test "$HIH" != "0"
then
    HIH1="History-Info: <sip:+${B}@t-mobile.com;user=phone;Reason=SIP>;index=1"
    HIH2="History-Info: <sip:+${VM}@t-mobile.com;user=phone;Reason=SIP;cause=486>;index=1.1"
    FHIH1=`grep "${HIH1}" $RESPF | wc -l`
    FHIH2=`grep "${HIH2}" $RESPF | wc -l`
    if
      test "$FHIH1" != "1" -o "$FHIH2" != "1"
    then
      echo "1:$TESTNUM${AS}:HIH Header is not as expected"
      FLG=1
    fi
fi
#CHECK REASON
if 
  test $CR -gt 0
then
  case $ReasonCode in
       403)
           if
             test $CR -eq 1
           then
             RH="Reason: SIP;cause=403;text=\"Forbidden\""
           else
             RH="Reason: SIP;cause=403;text=\"Stale Date\""
           fi
           ;;
       486)
           RH="Reason: SIP;cause=486;text=\"Busy Here\""
           ;;
       *)
           echo "1:$TESTNUM${AS}:Unknown reason code"
           ;;
  esac
  if
    test "$RH" != ""
  then
    RHH=`grep "${RH}" $RESPF | wc -l`
    if
      test $RHH -lt 1
    then
      echo "1:$TESTNUM${AS}:Reason Header is not as expected"
      FLG=1
    fi
  fi
  QTYR=`grep -c "^Reason" $RESPF`
  if
     test $QTYR -ne 1
  then
    echo "1:$TESTNUM${AS}:There should be 1 and only 1 Reason Header"
    FLG=1
  fi
fi
   ;;
  # REST ************************************************
  rest | mpnb)
    ENDRESPL=`cat ${LOGNAME} | wc -l`
    tail -n +${STRESPL} $LOGNAME | head -n $(($ENDRESPL - $STRESPL + 1)) > $RESPF
    #CHECK action
    ACTRESP=`grep action $RESPF`
    case "$ACTRESP" in
      *${ACT}*)
          if
            test "$ACT" = "Forward"
          then
            FWDRESP=`grep forwardNumber $RESPF | cut -f2 -d">" | cut -f1 -d"<"`
            case "$FWDN" in
              "") if
                    test "$FWDRESP" != ""
                  then
                    echo "1:$TESTNUM${AS}:forwardNumber tag should be blank"
                    FLG=1
                  fi
                  ;;
              *) if
                    test "$FWDRESP" != "${URI}:+${FWDN}"
                  then
                    echo "1:$TESTNUM${AS}:forwardNumber tag has incorrect number"
                    FLG=1
                  fi
                  ;;
            esac
          fi
          #echo we have the right <action> tag
          ;;
      *)
          echo "1:$TESTNUM${AS}:action tag \"$ACTRESP\" is not as  expected"
          FLG=1
          ;;
    esac
    if
      test "$PROT" = "mpnb"
    then
      #CHECK reasonCode TDR only?
      REASRESP=`grep errorCode $RESPF`
      case "$REASRESP" in
        *${REASC}*)
            #echo we have the right <reasonCode> tag
            ;;
        *)
            echo "1:$TESTNUM${AS}:reason tag \"$REASRESP\" is not as  expected"
            FLG=1
            ;;
      esac
    fi
    #CHECK tag
    TAGRESP=`grep displayName $RESPF`
    case "$TAGRESP" in
      *${TAG}*)
          #echo we have the right <displayName> tag
          ;;
      *)
          echo "1:$TESTNUM${AS}:displayName tag \"$TAGRESP\" is not as  expected"
          FLG=1
          ;;
    esac
    case "$URI" in
      priv | tel)
        URI1='tel:+'
        URI2='tel:+'
        ;;
      sip)
        URI1='sip:+'
        URI2='sip:+'
        ;;
      mailto)
        URI1='mailto:'
        URI2='tel:+'
        ;;
     esac
       for CALLTAG in `echo "callingParticipant>${URI1}${A}" "calledParticipant>${URI2}${B}"`
       do
         CNT=`grep $CALLTAG $RESPF | wc -l`
         if
           test $CNT -ne 1
         then
           echo "1:$TESTNUM${AS}:$CALLTAG not as expected"
           FLG=1
         fi
       done
     if
       test $EC -eq 1
     then
        RESPEC=`grep errorCode $RESPF | cut -f2 -d">" | cut -f1 -d"<"`
        if
          test "$RC" != "$RESPEC"
        then
          echo "1:$TESTNUM${AS}:errorCode $RESPEC not as expected"
        fi
     fi
     ;;
  *)
    echo "1:$TESTNUM${AS}:Protocol not expected"
    ;;
esac
fi
DIFFLG=`./diff_log.sh $TESTNUM${AS} | cut -f1 -d":"`
DIFFLG=0
if
  test "$DIFFLG" = "1" 
then
  echo "1:$TESTNUM${AS} - Differences exist between this test and the saved template"
fi
if
  test "$FLG" != "1" -a "$DIFFLG" != "1"
then
  echo "0:$TESTNUM${AS}:Passed"
fi
if
  test $CHKVS -eq 1
then
  VS_RESPF="/tmp/$$.${TESTNUM}-vs_resp"
  STRESPL=`grep -n -m2 "${RSPC}${RSPH}" ${LOGNAME} | tail -n 1 | cut -f1 -d":"`
  ENDRESPL=`sed -n "$STRESPL,$ p" $LOGNAME | grep -n -m 1 "Content-Length: 0" | cut -f1 -d":"`
  ENDRESPL=$(($STRESPL + ENDRESPL - 1))
  if
    test "${STRESPL}" = "" -o "${ENDRESPL}" = ""
  then
    echo "1:$TESTNUM:Cannot find STI-VS $RSPC response"
    exit
  fi
  sed -n "$STRESPL,$ENDRESPL p" $LOGNAME > $VS_RESPF
  cp $VS_RESPF ./logs/${TESTNUM}-vs.log
  $DIR/check_test.sh ${TESTNUM}-vs 
  #$DIR/check_test.sh ${TESTNUM}-vs $VS_RESPF
fi
rm -f $RESPF
