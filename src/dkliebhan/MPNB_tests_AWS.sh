echo "TEST1 - Continue" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+16303060003</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST2 - UserRule Reject" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-2</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147970</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST8 - scamoff - release" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551550</calledParticipant><callingParticipant>tel:+11212121216</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST9 - Continue - SIP numbers" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>sip:+16304147970@awslab.com</calledParticipant><callingParticipant>sip:+16303060003@awslab.com</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST10 - 404 code 10" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16314147970</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST11 - 404 code 20" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551543</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST12 - 404 code 30" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551542</calledParticipant><callingParticipant>tel:+11212121213</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST13 - 404 code 40" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147973</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST14 - Continue - Reason 70 - B not MPNB Sub"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16302101980</calledParticipant><callingParticipant>tel:+11212121213</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST15 - 405 GET to screening "
curl -i -X GET -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening
echo
echo "TEST16 - json" 
curl -i -X POST -H "Content-Type: application/json" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'{ test: "badrobot" }'
echo
echo "TEST17 - 400 invalid resource" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v2/msgScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+16303060003</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST18 - 400 missing Parameter " 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST19 - 400 invalid flavor" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+16303060003</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><dfk/></flavors></tpc:msgEventNotification>'
echo
echo "TEST20 - 403 bad server root" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/rstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+16303060003</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST21 - skip"
echo "TEST22 - skip"
echo
echo "TEST23 - Continue - no Reject - Carrier ID is not PNB Enabled"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551549</calledParticipant><callingParticipant>tel:+16304147970</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST24 - Forward to Voicemail with Carrier Number"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551548</calledParticipant><callingParticipant>tel:+11212121213</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'

echo
echo "TEST25 - PRIVATE display Scam Likely"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>priv:+11212121212</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST26 - PRIVATE block" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-26</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>priv:+16304147970</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST27 - PRIVATE catagory - display private on phone - response is Telemarketer with Private ind in TDR" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>priv:+12488003014</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST28 - display Scam Likely - SCAM > 11 digs"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-28</callSessionIdentifier><calledParticipant>tel:+16302101979</calledParticipant><callingParticipant>tel:+11212121213456</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST29 - Continue COMMTYPE MATCH" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147974</callingParticipant><addressDirection>Called</addressDirection><serviceKey>MMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST30 - UserRule Reject COMMTYPE MATCH" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147974</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST31 - block text only - voice call"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-31</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147971</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST32 - block text only - text message"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-33</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147971</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST33 - block voice only - voice call"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-33</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147972</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST34 - block voice only - text message"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-34</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147972</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST35 - voice forward UserRule text processed normally - overriding globalDefault" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-35</callSessionIdentifier><calledParticipant>tel:+16302101979</calledParticipant><callingParticipant>tel:+14044559111</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST36 -  - globalDefault text processed normally" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-35</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+14044559111</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST37 - sent matching COMMTPE and BLOCK all on"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147970</callingParticipant><addressDirection>Called</addressDirection><serviceKey>MMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST38 - sent matching COMMTPE and BLOCK all text on"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147971</callingParticipant><addressDirection>Called</addressDirection><serviceKey>MMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
curl -i -H "Accept:" -X GET http://172.22.43.180:8080/firstorion.com/msgScreening/v1/healthCheck
echo
echo "TEST39 - email"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>mailto:kliebhan@gmail.com</callingParticipant><addressDirection>Called</addressDirection><serviceKey>MMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST41 - scamoff - Carrier TExt block FAlse - continue"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551547</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST123 - valid healthCheck - no headers" 
echo "TEST124 - 405 POST to healthCheck" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/healthCheck
echo
echo "TEST125 - skip"
echo "TEST126 - skip"
echo "TEST127 - skip"
echo "TEST128 - skip"
echo "TEST129 - valid healthCheck - headers" 
curl -i -H "Accept:application/xml" -X GET http://172.22.43.180:8080/firstorion.com/msgScreening/v1/healthCheck
echo
