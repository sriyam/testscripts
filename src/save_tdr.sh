#!/bin/bash

change_line () {
   CHG=$1
   CHGWHAT=$2
   TOWHAT=$3
   FILE=$4
   TMPFILE=/tmp/$$.chgline
   case $CHG in
     repl)
       MODL=$TOWHAT
       ;;
     chgv)
       cat $FILE | sed -e "s/${CHGWHAT}/${TOWHAT}/" > $TMPFILE
       MODL=""
       mv $TMPFILE $FILE
       ;;
     chgg)
       cat $FILE | sed -e "s/${CHGWHAT}/${TOWHAT}/g" > $TMPFILE
       MODL=""
       mv $TMPFILE $FILE
       ;;
   esac
   if
     test "$MODL" != ""
   then
     MODN=`grep -m 1 -n "${CHGWHAT}" $FILE | cut -f1 -d":"`
     if
       test "$MODN" != ""
     then
       N=`expr $MODN \- 1`
       head -$N $FILE > $TMPFILE
       echo $MODL >> $TMPFILE
       N=`expr $MODN \+ 1`
       tail -n +$N $FILE >> $TMPFILE
       mv $TMPFILE $FILE
     fi
   fi
}
DIR=$HOME
TESTDATA=`grep ^${1}: $DIR/tests.data`
if
  test "$TESTDATA" = ""
then
  echo "need valid logname to save"
  exit
fi
TESTNUM=`echo $TESTDATA | cut -f1 -d":"`
C4=`echo $TESTNUM | cut -c1-4`
TDRF="$DIR/logs/${TESTNUM}.tdr"
if
  test ! -f $TDRF
then
  echo "1:$TESTNUM:no source log file to prep as template"
  exit
fi
USER=`echo $HOME | cut -f3 -d"/"`
RESPLOG=/tmp/$$.RESPLOG
USELOG=/tmp/$$.logname
TEMPLNAME="./diffdir/${C4}/${TESTNUM}.tdr"
case $C4 in
  rest | mpnb)
    # PNB and MPNB
    #STRESPL=`grep -n -m1 "^HTTP" ${USELOG} | cut -f1 -d":"`
    #ENDRESPL=`cat ${USELOG} | wc -l`
    ;;
  *)
    # SIP ************************************************
    EDGE=`grep DELEGATOR $TDRF | cut -f3 -d"|"`
    CALLID=`grep DELEGATOR $TDRF | cut -f7 -d"|"`
    TIMESTAMP=`grep DELEGATOR $TDRF | cut -f8 -d"|"`
    CONTEXT=`grep DELEGATOR $TDRF | cut -f9 -d"|"`
    SRCIP=`grep DELEGATOR $TDRF | cut -f11 -d"|"`
    DSTIP=`grep DELEGATOR $TDRF | cut -f12 -d"|"`
    PORT=`grep DELEGATOR $TDRF | cut -f13 -d"|"`
    #STRESPL=`grep -n -m1 "302 Moved Temporarily" ${USELOG} | cut -f1 -d":"`
    #if
    #  test "$STRESPL" = ""
    #then 
    #  STRESPL=`grep -n -m1 "487 Request Terminated" ${USELOG} | cut -f1 -d":"`
    #fi
    #ENDRESPL=`grep -n -m1 "Content-Length: 0" ${USELOG} | cut -f1 -d":"`
    #;;
esac
#if
#  test "${STRESPL}" = "" -o "${ENDRESPL}" = ""
#then
#  echo "1:$TESTNUM:Cannot find response payload to save"
#  exit
#else
#  tail -n +${STRESPL} $USELOG | head -n $(($ENDRESPL - $STRESPL + 1)) > $RESPLOG
  case $C4 in
    rest | mpnb)
      change_line repl '^Date:' 'Date: XXXXXX' $RESPLOG
      change_line chgv '>123456-.*<' '>123456<' $RESPLOG
      ;;
    *)
      cut -f2- -d"|" $TDRF | \
         sed -e "s/$EDGE/EDGE/" | \
         sed -e "s/$CALLID/CALLID/g" | \
         sed -e "s/$TIMESTAMP|$CONTEXT/TIMESTAMP|CONTEXT/" | \
         sed -e "s/$CONTEXT/CONTEXT/" | \
         sed -e "s/$SRCIP/SRCIP/" | \
         sed -e "s/$DSTIP/DSTIP/" | \
         sed -e "s/|$PORT|/|PORT|/" > $RESPLOG

      #change_line chgv ';branch=.*;' ';branch=XXXXX;' $RESPLOG
      #change_line chgv ';branch=.*-0$' ';branch=XXXXX' $RESPLOG
      #change_line chgv 'tag=.*$' 'tag=YYYYY' $RESPLOG
      #change_line chgv 'Call-ID: .*@' 'Call-ID ZZZZZ@' $RESPLOG
      #change_line chgv "$SRCIP" 'SRCIP' $RESPLOG
      #change_line chgv ":${SRCPORT}" ':SRCPORT' $RESPLOG
      #change_line chgv "=${SRCPORT}" '=SRCPORT' $RESPLOG
      #change_line chgv "$DSTIP" 'DSTIP' $RESPLOG
      #change_line chgv ":${DSTPORT}" ':DSTPORT' $RESPLOG
      #change_line chgv '^Identity: .*;' 'Identity: IIIII;' $RESPLOG
      #change_line repl '^P-Origination-Id:' 'P-Origination-Id: HHHHH' $RESPLOG
      #change_line repl '^Date:' 'Date: DDDDD' $RESPLOG
      ;;
  esac
  #cat $RESPLOG
  mv $RESPLOG $TEMPLNAME
  echo "log for test $TESTNUM saved as new template"
#fi
rm -f /tmp/$$.*
