if
  test "$1" = "orch"
then
  case $2 in
    noid)
      grep ^${1} tests.data | cut -f1 -d":" | grep -v "s$" > /tmp/$$.testnums
      ;;
    id)
      grep ^${1} tests.data | cut -f1 -d":" | grep "s$" > /tmp/$$.testnums
      ;;
    *)
      grep ^${1} tests.data | cut -f1 -d":" > /tmp/$$.testnums
      ;;
  esac
else
  grep ^${1} tests.data | cut -f1 -d":" > /tmp/$$.testnums
fi
for I in `cat /tmp/$$.testnums`
do
  echo $I
  diff_tdr.sh $I y
done
