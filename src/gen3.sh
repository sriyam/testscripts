# LABS
FOAWS=172.22.43.180
#
SRCIP=172.22.23.173
SRCPORT=9090
DSTIP=$FOAWS
DSTPORT=7979
if
  test "${1}" = ""
then
  TEST=""
else
  #case ${1} in
  #  INFORM*)
  #    TEST=`grep ^${1}: inform_tests.data`
  #    ;;
  #  *)
  #    TEST=`grep ^${1}: tests.data`
  #    ;;
  #esac
  TEST=`grep ^${1}: tests.data`
fi
if
  test "$TEST" = ""
then
  echo "Usage: gen2.sh scam001 [TYPE] - to send an INVITE to Delegator"
  echo "valid test cases are: "
  tail -n+2 *.data | cut -f1 -d":" 
  exit
fi
TESTNUM=`echo $TEST | cut -f1 -d":"`
A=`echo $TEST | cut -f2 -d":"`
B=`echo $TEST | cut -f3 -d":"`
PAI=`echo $TEST | cut -f4 -d":"`
PI=`echo $TEST | cut -f5 -d":"`
if
  test "$2" != ""
then
  TYPE=$2
else
  case $PAI in
    "") case $PI in
        PR) TYPE=emer.xml
	   ;;
        PI) TYPE=pi.xml
	   ;;
	*) TYPE=base.xml
	   ;;
      esac
      ;;
    PAI) case $PI in
        PR) TYPE=pai_emer.xml
	   ;;
        PI) TYPE=pai_pi.xml
	   ;;
	*) TYPE=pai.xml
	   ;;
      esac
      ;;
    *) # PAI and A do not match
       SPAI=$PAI
       case $PI in
        PR) TYPE=pai_emer.xml
	   ;;
        PI) TYPE=pai_pi.xml
	   ;;
        TO) TYPE=diff_to.xml
	   ;;
	*) TYPE=diff_pai.xml
	   ;;
      esac
      ;;
  esac
fi
TYPE=pai_cydl.xml
TYPE=cydl.xml
if
  test "`echo $TESTNUM | cut -c1-4`" = "stir"
then
  cp $TYPE xml/sti-as.xml
  TYPE=sti-as.xml
fi
PID=`./call_delegator.sh $SRCIP $SRCPORT $DSTIP $DSTPORT $A $B $TYPE $SPAI`
mv /tmp/$PID.log ./logs/$TESTNUM.log
cat ./logs/$TESTNUM.log
