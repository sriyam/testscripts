#IP=172.22.43.180
IP=172.22.43.39
echo "test - 1 DefaultAction	999901"
dig @${IP} -t naptr 0.7.9.7.4.1.4.0.3.6.1.$.4.3.5.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 2 SpamRule	999999"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.4.3.5.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 3 SpamRule	999902"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.9.7.9.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 4 GlobalDefault	999910"
dig @${IP} -t naptr 1.1.1.9.5.5.4.4.0.4.1.$.0.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 5 Transit		999990"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.9.5.2.2.9.7.9.0.3.6.1.e164.arpa
echo "test - 6 GlobalOveride	999999"
dig @${IP} -t naptr 2.1.2.1.5.5.5.0.3.6.1.$.4.3.5.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 7 NoProfile	999901"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.1.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 8 CarrierDisable	999901"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.2.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 9 CarrierMissing	999901"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.3.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 10 ProfileInactive	999901"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.4.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 11 UserRule	999999"
dig @${IP} -t naptr 0.7.9.7.4.1.4.0.3.6.1.$.0.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 12 UserRule	999901"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.0.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 13 UserRule	999980"
dig @${IP} -t naptr 1.1.1.9.5.5.4.4.0.4.1.$.9.7.9.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 14 SpamRule - Priv	999902"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.9.9.9.9.9.$.9.7.9.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 15 UserRule - Priv	999999"
dig @${IP} -t naptr 0.7.9.7.4.1.4.0.3.6.1.9.9.9.9.9.$.0.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 16 SpamRule	999980"
dig @${IP} -t naptr 1.7.0.8.5.8.4.0.8.9.1.$.0.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 17 SpamRule	999911"
dig @${IP} -t naptr 6.0.0.2.3.9.6.8.1.2.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 18 SpamRule	999912"
dig @${IP} -t naptr 4.1.0.3.0.0.8.8.4.2.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 19 SpamRule	999913"
dig @${IP} -t naptr 7.6.0.8.5.8.4.0.8.9.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 20 SpamRule	999914"
dig @${IP} -t naptr 7.3.0.3.0.0.8.8.4.2.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 21 SpamRule	999915"
dig @${IP} -t naptr 1.0.0.9.4.6.7.1.0.5.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 22 SpamRule	999916"
dig @${IP} -t naptr 8.0.0.7.5.0.6.2.6.6.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 23 SpamRule	999917"
dig @${IP} -t naptr 8.6.0.8.5.8.4.0.8.9.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 24 SpamRule	999918"
dig @${IP} -t naptr 9.6.0.8.5.8.4.0.8.9.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 25 SpamRule	999919"
dig @${IP} -t naptr 0.7.0.8.5.8.4.0.8.9.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 26 SpamRule	999920"
dig @${IP} -t naptr 1.7.0.8.5.8.4.0.8.9.1.$.0.7.9.7.4.1.4.0.3.6.1.e164.arpa
echo "test - 27 SpamRule	999901"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.0.8.9.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 28 UserRule	999999"
dig @${IP} -t naptr 0.7.9.7.4.1.4.0.3.6.1.$.0.8.9.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 29 ProfileInactive	999901"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.1.8.9.1.0.1.2.0.3.6.1.e164.arpa
echo "test - 30 DefaultAction	999901"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.6.4.5.1.5.5.7.0.3.6.1.e164.arpa
echo "test - 31 NXDOMAIN"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.$.0.4.5.1.5.5.7.0.3.1.1.e164.arpa
echo "test - 32 REFUSED"
dig @${IP} -t naptr 2.1.2.1.2.1.2.1.2.1.1.y.0.4.5.1.5.5.7.0.3.1.1.e164.arpa
