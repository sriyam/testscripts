DIR=$HOME
NEWINVF=/tmp/$$.newINV
TMPINVF=/tmp/$$.tmpINV
RESP302F=/tmp/$$.resp302
CUSTOM=1
LOGNAME=$1
if
  test ! -r $LOGNAME
then
  echo "no file $LOGNAME" >> $LOGNAME
  rm -f $DIR/xml/sti-vs.xml
  exit 
else
  LC=`cat $LOGNAME | wc -l`
  if
    test $LC -lt 5
  then
    echo "something went wrong" >> $LOGNAME
    rm -f xml/sti-vs.xml
    exit
  fi
fi
if
  test "$2" != ""
then
  CUSTOM=$2
fi
NEEDPAI=`grep 'P-Asserted-Identity:' $LOGNAME | wc -l`
STRRESPL=`grep -n -m1 "302 Moved Temporarily" ${LOGNAME} | cut -f1 -d":"`
if
  test "$STRRESPL" = ""
then
  echo "NO 302 in AS response" >> $LOGNAME
  rm -f xml/sti-vs.xml
  exit
fi
ENDRESPL=`grep -n -m1 "Content-Length: 0" ${LOGNAME} | cut -f1 -d":"`
tail -n +$STRRESPL $LOGNAME | head -n $(($ENDRESPL - $STRRESPL + 1)) | tr -d '\r' > $RESP302F
case $CUSTOM in
  0)
    if
      test "$NEEDPAI" = "0"
    then
      cp $DIR/xml/stivs_sipp.xml $NEWINVF
    else
      cp $DIR/xml/stivs_pai_sipp.xml $NEWINVF
    fi
	;;
  1)
    if
      test "$NEEDPAI" = "0"
    then
      #cp $DIR/xml/stivs_custom_sipp.xml $NEWINVF
      cp $DIR/xml/stivs_custom_2pai_sipp.xml $NEWINVF
    else
      #cp $DIR/xml/stivs_custom_pai_sipp.xml $NEWINVF
      cp $DIR/xml/stivs_custom_2pai_sipp.xml $NEWINVF
    fi
	;;
  2)
    if
      test "$NEEDPAI" = "0"
    then
      cat $DIR/xml/stivs_sipp.xml | sed -e "s/23:5060/24:5060" > $NEWINVF
    else
      cp $DIR/xml/stivs_pai_sipp.xml $ | sed -e "s/23:5060/24:5060" >NEWINVF
    fi
	;;
  *)
	;;
esac
for OPER in `echo add modify delete`
do
  HDRL=`grep ^"P-STI-Header-Info: ${OPER}" $RESP302F | cut -f2 -d"="`
  while
     test "$HDRL" != ""
  do
     echo $HDRL >> /tmp/$$.hdrs
     HDR=`echo $HDRL | cut -f1 -d","`
     echo $OPER:$HDR >> /tmp/$$.hdrs
     HDRL=`echo $HDRL | cut -s -f2- -d","`
     if
       test "$HDR" != ""
     then
       case $OPER in
         add)
           ADDL=`grep ^${HDR} $RESP302F`
      echo ADDL:$ADDL >> /tmp/$$.hdrs
           CSEQL=`grep -m 1 -n -i CSeq $NEWINVF | cut -f1 -d":"`
      echo CESQL:$CSEQL >> /tmp/$$.hdrs
           N=`expr $CSEQL \- 1`
      echo N:$N >> /tmp/$$.hdrs
           head -$N $NEWINVF > $TMPINVF
           echo $ADDL >> $TMPINVF
           tail -n +$CSEQL $NEWINVF >> $TMPINVF
      cat $TMPINVF >> /tmp/$$.hdrs
           #cat $TMPINVF
	   mv $TMPINVF $NEWINVF
           ;;
         modify)
           MODL=`grep ^${HDR} $RESP302F`
           #LINECNT=`grep -o ${HDR} $RESP302F | wc -l`
           #if
           #  test $LINECNT -gt 1
           #then
           #  MODL=`echo $MODL | sed -e "s/ ${HDR}/\n${HDR}/"`
           #fi
           MODN=`grep -m 1 -n ${HDR} $NEWINVF | cut -f1 -d":"`
           N=`expr $MODN \- 1`
           head -$N $NEWINVF > $TMPINVF
           echo $MODL >> $TMPINVF
           N=`expr $MODN \+ 1`
           tail -n +$N $NEWINVF >> $TMPINVF
           #cat $TMPINVF
	   mv $TMPINVF $NEWINVF
           ;;
         delete)
           DELL=`grep ^${HDR} $RESP302F`
           DELN=`grep -m 1 -n ${HDR} $NEWINVF | cut -f1 -d":"`
           N=`expr $DELL \- 1`
           head -$N $NEWINVF > $TMPINVF
           N=`expr $DELL \+ 1`
           tail -n +$N $NEWINVF >> $TMPINVF
           #cat $TMPINVF
	   mv $TMPINVF $NEWINVF
           ;;
         *)
           ;;
       esac
     fi
  done
done
#echo
#POST work
#OLDTS=`grep Call-ID $RESP302F | cut -f3 -d"-" | cut -f1 -d"@"` 
#cat $NEWINVF | sed -e "s/$OLDTS/XXXXXX/" > invites/INVITE.stivs
mv $NEWINVF xml/sti-vs.xml
rm -f $RESP302F
rm -f /tmp/$$.hdrs
