#!/bin/bash
# Reference:  TMOB-2123
#
# Description:  This script will export and parse the DELEGATOR_FRAMEWORK.E911_SUPRESSION_LIST view for stale records.
#               Stale records are defined as entries that have to been expunched from the E911_SUPPRESION_LIST for some
#               reason or other.   
#
# Usage:  Manually execute the script as admin user on titan and follow the instructions from the output to perform cleanup.
#
#         As admin user:
#             1) copy e911_cleanup.sh to /home/admin
#             2) cd /home/admin
#             3) ./e911_cleanup.sh
#
# CRON:  To automate the scripts execution, install it into root crontab.
#
#        As root user:
#            1) crontab -e
#            2) Add this line to crontab:
#
#                   58 0 * * * su - admin -c "cd /home/admin; /home/admin/e911_cleanup.sh clean"
#
 
function show_time () {
    num=$1
    min=0
    hour=0
    day=0
    if((num>59));then
        ((sec=num%60))
        ((num=num/60))
        if((num>59));then
            ((min=num%60))
            ((num=num/60))
            if((num>23));then
                ((hour=num%24))
                ((day=num/24))
            else
                ((hour=num))
            fi
        else
            ((min=num))
        fi
    else
        ((sec=num))
    fi
    echo "$day"d "$hour"h "$min"m "$sec"s
}

# SCRIPT Variables
epoch_time=`date +%s`
delegator=IRVSPC62
timer=1500 # 3 times current timer
logfile="e911_cleanup.log"
c=0

# Main script

#create log file if does not exist
if
        test ! -f "$logfile"
then
        echo "`date`: file created" > $logfile
fi

#export DELEGATOR_FRAMEWORK.E911_SUPRESSION_LIST view
/opt/titan/bin/entry-export DELEGATOR_FRAMEWORK.E911_SUPRESSION_LIST $$.view

#Parse view for stale records
echo "`date`: e911_cleanup script executed" >> $logfile
echo "-----------------"
echo "current epoch: $epoch_time"
echo "timer: $timer"
echo "-----------------"
echo "b_party:end_time:stale_time"
while read -r line
do
	# extract relevant info from view
	#
	# SAMPLE:  b_party='19896273389' end_time=1575585859286 e911_supression_list {start_time=1575578659286};
	#
	b_party=`echo $line | cut -f2 -d"'"`
	epoch_end_time=`echo $line | cut -f3 -d= |cut -c1-10`
	end_time=`echo $line | cut -f3 -d= |cut -c1-13`
	start_time=`echo $line | cut -f4 -d= |cut -c1-13`
	diff_time=$(($epoch_time-$epoch_end_time))

	if [ "$diff_time" -gt "$timer" ]; then
		echo "$b_party:`show_time $diff_time`"
		echo "`date`: stale record found, $b_party:`show_time $diff_time`" >> $logfile
		echo "edge_name=\"$delegator\" primary_indicator=1 end_time=$end_time b_party='$b_party' e911_callers_to_bypass_list {start_time=$start_time last_event_id=\"cleanup_$epoch_time\"};" >> $$.cleanup
		c=$((c+1))
	fi

done < $$.view

echo "-----------------"
echo "Total stale e911 records: $c"
echo "`date`: Total stale e911 records: $c" >> $logfile
echo ""

if 
	test $c -gt 0 -a "$1" = "clean"
then
	echo "cleaning... with $$.cleanup"
	#/opt/titan/bin/entry-import DELEGATOR_FRAMEWORK.E911_CALLERS_TO_BYPASS_LIST $$.cleanup
	echo "`date`: auto cleanup of $$.cleanup" >> $logfile
	echo "list of records:" >> $logfile
	cat $$.cleanup >> $logfile
	rm $$.cleanup
else
	echo "To clean up the stale records execute: /opt/titan/bin/entry-import DELEGATOR_FRAMEWORK.E911_CALLERS_TO_BYPASS_LIST $$.cleanup"
	echo "`date`: manual clean up the stale records execute: /opt/titan/bin/entry-import DELEGATOR_FRAMEWORK.E911_CALLERS_TO_BYPASS_LIST $$.cleanup" >> $logfile
fi

rm $$.view
