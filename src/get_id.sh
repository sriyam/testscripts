DIR=$HOME
LOGNAME=${1}
NEWINVF=${2}
TMPINVF=/tmp/$$.tmpinv
if
  test ! -f $LOGNAME
then
  HDR="Error: no output file from which to get Identity"
else
  STRESPL=`grep -n -m1 "302 Moved Temporarily" ${LOGNAME} | cut -f1 -d":"`
  ENDRESPL=`grep -n -m1 "Content-Length: 0" ${LOGNAME} | cut -f1 -d":"`
  if
    test "${STRESPL}" = "" -o "${ENDRESPL}" = "" 
  then
    HDR="Error: No response not as expected"
  else
    #GET Identity
    HDR=`tail -n +${STRESPL} $LOGNAME | head -n $(($ENDRESPL - $STRESPL + 1)) | grep "^Identity:" $RESPF | tr -d '\r'`
    #HDR=`tail -n +${STRESPL} $LOGNAME | head -n $(($ENDRESPL - $STRESPL + 1)) | grep "^Identity:" $RESPF | sed -e "s/;alg=ES256//" | tr -d '\r'`
    #HDR=`tail -n +${STRESPL} $LOGNAME | head -n $(($ENDRESPL - $STRESPL + 1)) | grep "^Identity:" $RESPF  | sed -e "s/;ppt=shaken//" | tr -d '\r'`
    #HDR=`tail -n +${STRESPL} $LOGNAME | head -n $(($ENDRESPL - $STRESPL + 1)) | grep "^Identity:" $RESPF | sed -e "s/;alg=ES256//" | sed -e "s/;ppt=shaken//" | tr -d '\r'`
  fi
fi
if
  test "$HDR" = ""
then
  HDR="Identity: NoIdentityReturned"
  echo "NO IDENTITY RETURNED - you have 5 seconds to kills this or just let it continue"
  sleep 5
fi
#echo $HDR
CSEQL=`grep -m 1 -n -i CSeq $NEWINVF | cut -f1 -d":"`
#echo CESQL:$CSEQL >> /tmp/$$.hdrs
N=`expr $CSEQL \- 1`
#echo N:$N >> /tmp/$$.hdrs
head -$N $NEWINVF > $TMPINVF
echo $HDR >> $TMPINVF
tail -n +$CSEQL $NEWINVF >> $TMPINVF
#cat $TMPINVF >> /tmp/$$.hdrs
#cat $TMPINVF
mv $TMPINVF $NEWINVF
#cat $TMPINVF | sed -e "s/sip:+/sip:/g" > $NEWINVF
