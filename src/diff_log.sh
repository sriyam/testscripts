#!/bin/bash

change_line () {
   CHG=$1
   CHGWHAT=$2
   TOWHAT=$3
   FILE=$4
   TMPFILE=/tmp/$$.chgline
   case $CHG in
     repl)
       MODL=$TOWHAT
       ;;
     chgv)
       cat $FILE | sed -e "s/${CHGWHAT}/${TOWHAT}/" > $TMPFILE
       #GETL=`grep "${CHGWHAT}" $FILE`
       #if
       #  test "$GETL" != ""
       #then
       #  MODL=`echo $GETL | sed -e "s/${CHGWHAT}/${TOWHAT}/"`
       #else
       MODL=""
       mv $TMPFILE $FILE
       #fi
       ;;
     chgg)
       cat $FILE | sed -e "s/${CHGWHAT}/${TOWHAT}/g" > $TMPFILE
       MODL=""
       mv $TMPFILE $FILE
       ;;
   esac
   if
     test "$MODL" != ""
   then
     MODN=`grep -m 1 -n "${CHGWHAT}" $FILE | cut -f1 -d":"`
     if
       test "$MODN" != ""
     then
       N=`expr $MODN \- 1`
       head -$N $FILE > $TMPFILE
       echo $MODL >> $TMPFILE
       N=`expr $MODN \+ 1`
       tail -n +$N $FILE >> $TMPFILE
       mv $TMPFILE $FILE
     fi
   fi
}

DIR=$HOME
RESPF=/tmp/$$.RESPF
USELOG=/tmp/$$.logname
TESTDATA=`grep ^${1}: tests.data`
VERBOSE=${2}
if
  test "$TESTDATA" = ""
then
  echo "cannot diff without a valid test number"
  exit
fi
TESTNUM=`echo $TESTDATA | cut -f1 -d":"`
LOGNAME="$DIR/logs/${TESTNUM}.log"
if
  test ! -f $LOGNAME
then
  echo "1:$TESTNUM:no source log file to use for diff - run nxtgen $TESTNUM"
  exit
else
  tr -d "\r" < $LOGNAME > $USELOG
fi
USER=`echo $HOME | cut -f3 -d"/"`
LABIPPORT=`grep -m 1 ^INVITE $USELOG | cut -f2 -d"@" | cut -f1 -d";"`
if
  [[ $LABIPPORT =~ msg.pc.t-mobile.com.*$ ]]
then
  LABIPPORT=`grep -m 1 ^To: $USELOG | cut -f2 -d"@" | cut -f1 -d";"`
fi
SRCIPPORT=`grep -m 1 $LABIPPORT $DIR/labconfig.txt | grep -v "^#" | grep $USER | cut -f4,5 -d":"`
SRCIP=`echo $SRCIPPORT | cut -f1 -d":"`
SRCPORT=`echo $SRCIPPORT | cut -f2 -d":"`
DSTIP=`echo $LABIPPORT | cut -f1 -d":"`
DSTPORT=`echo $LABIPPORT | cut -f2 -d":"`
if
  test "$SRCIP" = "" -o "$DSTIP" = ""
then
      echo "Invalid lab config - no src ip "${SRCIP}" or dest ip "${DSTIP}" found"
      exit
fi
C4=`echo $TESTNUM | cut -c1-4`
DIFFNAME="$DIR/diffdir/${C4}/${TESTNUM}.log"
if
  test ! -f $DIFFNAME
then
  echo "1:$TESTNUM:no diff log file to use for diff"
  exit
fi
case $C4 in
  rest | mpnb)
    # PNB and MPNB
    STRESPL=`grep -n -m1 "^HTTP" ${USELOG} | cut -f1 -d":"`
    ENDRESPL=`cat ${USELOG} | wc -l`
    ;;
  *)
    # SIP ************************************************
    STRESPL=`grep -n -m1 "302 Moved Temporarily" ${USELOG} | cut -f1 -d":"`
    if
      test "$STRESPL" = ""
    then 
      STRESPL=`grep -n -m1 "487 Request Terminated" ${USELOG} | cut -f1 -d":"`
    fi
    ENDRESPL=`grep -n -m1 "Content-Length: 0" ${USELOG} | cut -f1 -d":"`
    ;;
esac
if
  test "${STRESPL}" = "" -o "${ENDRESPL}" = ""
then
  echo "1:$TESTNUM:Cannot find start:${STRESPL} or end:${ENDRESPL} line in response payload"
  exit
else
  tail -n +${STRESPL} $USELOG | head -n $(($ENDRESPL - $STRESPL + 1)) > $RESPF
fi
case $C4 in
  rest | mpnb)
    # PNB and MPNB
    change_line repl '^Date:' 'Date: XXXXXX' $RESPF
    change_line chgv '>123456-.*<' '>123456<' $RESPF
    ;;
  *)
    # SIP ************************************************
    change_line chgv ';branch=.*;' ';branch=XXXXX;' $RESPF
    change_line chgv ';branch=.*-0$' ';branch=XXXXX' $RESPF
    change_line chgv 'tag=.*$' 'tag=YYYYY' $RESPF
    change_line chgv 'Call-ID: .*@' 'Call-ID ZZZZZ@' $RESPF
    change_line chgv "$SRCIP" 'SRCIP' $RESPF
    change_line chgv ":${SRCPORT}" ':SRCPORT' $RESPF
    change_line chgv "=${SRCPORT}" '=SRCPORT' $RESPF
    #change_line chgv 'msg.pc.t-mobile.com' 'DSTIP:DSTPORT' $RESPF
    change_line chgv "$DSTIP" 'DSTIP' $RESPF
    change_line chgv ":${DSTPORT}" ':DSTPORT' $RESPF
    change_line chgv '^Identity: .*;' 'Identity: IIIII;' $RESPF
    change_line repl '^P-Origination-Id:' 'P-Origination-Id: HHHHH' $RESPF
    change_line repl '^Date:' 'Date: DDDDD' $RESPF
    ;;
esac
RC="0"
diff $DIFFNAME $RESPF > /tmp/$$.difflog
DIFFCNT=`cat /tmp/$$.difflog | wc -l`
if
  test $DIFFCNT -gt 0
then
  grep "^<" /tmp/$$.difflog | cut -c3- > /tmp/$$.olddiff1
  grep "^>" /tmp/$$.difflog | cut -c3- > /tmp/$$.newdiff1
  diff /tmp/$$.olddiff1 /tmp/$$.newdiff1 > /tmp/$$.difflog2
  DIFFCNT2=`cat /tmp/$$.difflog2 | wc -l`
  if
    test $DIFFCNT2 -gt 0
  then
    RC="1"
  fi
fi
if
   test "${VERBOSE}" != "" 
then
   WC=0
   if
     test -f /tmp/$$.difflog2
   then
     WC=`cat /tmp/$$.difflog2 | wc -c`
   fi
   if
     test $WC -gt 0
   then
     cat /tmp/$$.difflog2
   else
     echo "no differences"
   fi
else
   echo $RC
fi
#cat $RESPF
rm -f /tmp/$$.*
