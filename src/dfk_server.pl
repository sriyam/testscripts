#!/usr/bin/perl

use strict;
use Socket;
use IO::Socket;
use IO::Socket::INET;
#use HTTP::Response;
#use HTTP::Request::Common;
#use LWP::UserAgent;
#use REST::Client;
#use URI::Escape;

# Simple web server in Perl

# Setup and create socket
open (PIDFILE, ">/home/dkliebhan/dfk_3pas.pid");
print PIDFILE "$$\n";
close (PIDFILE);

my %in;
my @in;
my $in;
my $key;
my $val;

# parse form variables to array
if ($#ARGV >= 0) {
	foreach my $arg (@ARGV) {
        	($key, $val) = split(/=/,$arg,2); # splits on the first =
        	push(@in,$key,$val);
	}
	%in=@in;
}

my $SHOST=$in{'shost'};
my $SPORT=$in{'sport'};
my $LHOST=$in{'lhost'};
my $LPORT=$in{'lport'};
my $INSTR=$in{'in_post'};
my $OTSTR=$in{'out_post'};
my $SUBFLG=$in{'subflg'};
my $DEBUG=$in{'debug'};

my $LOGFILE = "/home/dkliebhan/dfk_3pas.log";
my $URLFILE = "/applogs/prs_url.db";
my $SPECPATH = "/cgi/server/3pas_apps";

my %request;

if ($INSTR eq "") { $INSTR = 'urn:oma:xml:rest'; }
if ($OTSTR eq "") { $OTSTR = 'urn:oma:xml:rest:netapi'; }
if ($LHOST eq "") { $LHOST = '172.22.43.180'; }
if ($LPORT eq "") { $LPORT = 5060; }
if ($SHOST eq "") { $SHOST = '172.22.23.173'; }
if ($SPORT eq "") { $SPORT = 5061; }
if ($DEBUG eq "") { $DEBUG = 1; }
if ($SUBFLG eq "") { $SUBFLG = "prs910"; }

# Setup and create socket
my $DOCUMENT_ROOT = $ENV{'HOME'} . "/";
my($server) = IO::Socket::INET->new(
	Listen => 5,
	LocalAddr => $LHOST,
	LocalPort => $LPORT,
	Proto     => 'udp'
	);
$server or die "Unable to create server socket at $LHOST:$LPORT - $!" ;

##############################################
# Await requests and handle them as they arrive
##############################################
while (my $client = $server->accept()) {
    $client->autoflush(1);
    %request = ();
    my $RESPONSE = "";
    my $ContLen = "";
    my $SYSCMD = "";
    my $resp_type = "unknown";
    my $FIXED_RESP = "";
    my $TMPURL = "notset";
    my $SUBURL;
    my $DFLG = "none";
    eval {
      local $SIG{ALRM} = sub {die 'Timed Out'; };
      alarm 1;
	##############################################
	#-------- Read Request ---------------
	##############################################
        local $/ = Socket::CRLF;
        while (<$client>) {
            chomp; # Main http request
            if (/\s*(\w+)\s*([^\s]+)\s*HTTP\/(\d.\d)/) {
                $request{METHOD} = uc $1;
                $request{URL} = $2;
                $request{HTTP_VERSION} = $3;
            } # Standard headers
            elsif (/:/) {
                (my $type, my $val) = split /:/, $_, 2;
                $type =~ s/^\s+//;
                foreach ($type, $val) {
                        s/^\s+//;
                        s/\s+$//;
                }
                $request{lc $type} = $val;
            } # POST data
            elsif (/^$/) {
                read($client, $request{CONTENT}, $request{'content-length'})
                    if defined $request{'content-length'};
                last;
            }
        } # End while processing socket data
      alarm 0;
    }; # End eval
    alarm 0;
    ##############################################
    # Process Input request                      #
    ##############################################
    my $getdate = `date +"%D %T %Z"`;
    chomp($getdate);
    open(LOG, ">>$LOGFILE");
    print LOG "\n****NEW REQUEST **** TIME: $getdate\n";
    print LOG "Request URL: $request{URL}\n";
    if ($@ && $@ =~ /Timed Out/ ) {
        print LOG "This post timed out ... likely Content-Length issue\n";
        if (exists $request{'content-length'}) {
          print LOG "received Content-Length: $request{'content-length'} \n";
	} else {
           print LOG "No Content-Length received.\n";
    	}
    	close(LOG);
        close $client;
    } else {
    # send requests have no payload
    if (exists $request{CONTENT}) {
	print LOG "Request DATA: $request{CONTENT}\n";
    }
    if ( index($request{URL},"ParlayREST/")  > -1) {
	($RESPONSE,$ContLen,$TMPURL,$SUBURL,$DFLG)=&do_prs_req;
	$SYSCMD = "";
    } else {
	($RESPONSE,$ContLen,$SYSCMD)=&do_non_prs_req;
    }
    if ( index($RESPONSE,"application/xml") > -1) {
	$resp_type = "xml";
    } elsif ( index($RESPONSE,"application/json") > -1) {
	$resp_type = "json";
    }
    $FIXED_RESP=&fixup_response($resp_type,$RESPONSE,$ContLen,$DFLG);
    &sendback_final_resp($resp_type,$FIXED_RESP,$client,$RESPONSE);
    if ($TMPURL ne "notset") { 
    	# FIX 9to10 DB #
	&modify_db($resp_type,$RESPONSE,$TMPURL,$SUBURL,$request{URL}); 
    }
    if ($SYSCMD ne "") { 
	print LOG "\n**** system call: $SYSCMD\n";
	system("$SYSCMD &");
    }
    close(LOG);
    #############################
    # Close Connection and loop
    #############################
    close $client;
    }
}
    
sub sendback_final_resp {
##############
# Re-post Response
##############

my $TYPE = $_[0];
my $RESP = $_[1];
my $client = $_[2];
my $ORESP = $_[3];

print $client $RESP;
if ($DEBUG > 0) {
	print LOG "----> Post RESPONSE:\n";
	print LOG "**Orig Response**************\n";
	print LOG "$ORESP";
	print LOG "\n**New Response**************\n";
	print LOG "$RESP";
	print LOG "\n****************\n";
	print LOG "resp type: $TYPE \n";
	print LOG "****************\n";
}
} #END SUB

sub modify_db {

my $TYPE = $_[0];
my $RESP = $_[1];
my $TMPURL = $_[2];
my $SUBURL = $_[3];
my $RESPURL = $_[4];
my $EndrecID = 0;
my $StrrecID = 0;
my $lenid;
my $recID;

if ((index($RESP,"201 Created") != -1) ) {
	###############################################
	# BUILD DB ENTRY for successful subscriptions #
	###############################################
	if ($TYPE eq "xml") {
		$EndrecID = index($RESP,"</resourceURL>");
		$StrrecID = rindex($RESP,"/",$EndrecID) + 1;
	} elsif ($TYPE eq "json") {
		$EndrecID = index($RESP,"callbackReference") - 3;
		$StrrecID = rindex($RESP,"/",$EndrecID) + 1;
	}
	$lenid = $EndrecID - $StrrecID;
	$recID = substr($RESP, $StrrecID, $lenid);
    	if ($DEBUG > 0) {
		print LOG "adding db rec: $TMPURL;$SUBURL;$recID\n";
	}
    	open(DB, ">>$URLFILE");
    	print DB "$TMPURL;$SUBURL;$recID\n";
	close(DB);
} elsif (index($RESP,"204 No Content") != -1) {
	################################################
	# DELETE DB ENTRY for successful unsubscribes  #
	###############################################
	$EndrecID = length($RESPURL);
	$StrrecID = rindex($RESPURL,"/") + 1;
	$lenid = $EndrecID - $StrrecID;
	$recID = substr($RESPURL, $StrrecID, $lenid);
	my @fnd = &grepit($recID,$URLFILE,"not");
    	if ($DEBUG > 0) {
		print LOG "removing db rec: $recID\n New DB lines follow:";
		foreach (@fnd) {
    			print LOG "$_";
		}
	}
	####REBUILD DB
	open(DB, ">$URLFILE");
	foreach (@fnd) {
    		print DB "$_";
	}
	close(DB);
}
} #END SUB

sub do_prs_req {
##############################################
# PRS Processing                             #
# Do the request substitution per parameters #
##############################################
my $PAYLOAD_LEN = 0;
my $RESP = "";
my $NEWURL = "";
my $DATA = "";
my $DFLG = "none";
my $tmpURL = "notset";
my $SubURL = "";
my $WEBURL = "";
my $resp;

if ($SUBFLG eq "prs910") {
  if ($DEBUG > 0) {
	print LOG "\n***PRS910*******\n";
	#print LOG "Request URL: $request{URL}\n";
	#print LOG "Request DATA: $request{CONTENT}\n";
  }
  if ( index($request{URL},"/v1/") == -1) {
	$DFLG = "9to10";
	$NEWURL = &manipulate_post("url",$request{URL},"REST/1.0","REST");
       	my $POSCMD=index($NEWURL, "/", index($NEWURL,"REST/") + 5 );
       	my $URLSRT=substr($NEWURL, 0,$POSCMD);
       	my $URLEND=substr($NEWURL, $POSCMD);
	$NEWURL=$URLSRT . "/v1" . $URLEND;
	if ($DEBUG > 0) { print LOG "new url: $NEWURL\n"; }
	if ((index($request{CONTENT}, "callDirectionSubscription") != -1) || (index($request{CONTENT}, "callEventSubscription") != -1)) {
		$DFLG = "sub9to10";
		if ($request{'content-type'} eq "application/xml") {
			my $StrNfyURL = index($request{CONTENT},"<notifyURL>") + 11;
			my $EndNfyURL = index($request{CONTENT},"</notifyURL>");
			my $lenurl = $EndNfyURL - $StrNfyURL;
			$SubURL = substr($request{CONTENT}, $StrNfyURL, $lenurl);
			if ($DEBUG > 0) { print LOG "xml, $SubURL\n"; }
		} else {
			my $StrNfyURL = index($request{CONTENT},"notifyURL") + 13;
			my $EndNfyURL = index($request{CONTENT},"}",$StrNfyURL)- 1;
			my $lenurl = $EndNfyURL - $StrNfyURL;
			$SubURL = substr($request{CONTENT}, $StrNfyURL, $lenurl);
			if ($DEBUG > 0) { print LOG "json, $SubURL\n"; }
		}
		my $RNM=`date +%N | sha256sum | base64 | head -c8`;
		$tmpURL = "http://192.11.69.16:8080/ParlayREST/v1/prs910/" . $RNM;
		if ($DEBUG > 1) { print LOG "tmp url - $tmpURL\n"; }
		my $DATA0 = &manipulate_post("data",$request{CONTENT},$SubURL,$tmpURL);
		$DATA = &manipulate_post("data",$DATA0,$INSTR,$OTSTR);
	} else {
		$DATA = &manipulate_post("data",$request{CONTENT},$INSTR,$OTSTR);
	}
	if ($DEBUG > 0) { print LOG "flag : $DFLG\n"; }
  } elsif ( index($request{URL},"/prs910/") != -1) {
	# set flag such that we know we want prs9 format
	$DFLG = "urlout";
	$NEWURL = &manipulate_post($DFLG,$request{URL},"NA","NA");
	# opposite as this is outbound post to 3rd party app servers
	$DATA = &manipulate_post("data",$request{CONTENT},$OTSTR,$INSTR);
	$DATA = &manipulate_post("data",$DATA,"REST","REST/1.0");
	$DATA = &manipulate_post("data",$DATA,".14:8080",".16:8080");
	$DATA = &manipulate_post("data",$DATA,"/v1/","/");
	if ($DEBUG > 1) {
		print LOG "flag : $DFLG\n";
		print LOG "new data is \n $DATA \n";
	}
    	if ( $NEWURL eq "not_found" ) { $WEBURL = "fail"; }
	else { $WEBURL = $NEWURL; }
  } else {
	$NEWURL = $request{URL};
	$DATA = $request{CONTENT};
  }
} elsif ($SUBFLG eq "url") {
	$NEWURL = &manipulate_post("url",$request{URL},$INSTR,$OTSTR);
	$DATA = $request{CONTENT};
} else {
	$NEWURL = $request{URL};
	$DATA = &manipulate_post("data",$request{CONTENT},$INSTR,$OTSTR);
}
if (!defined($DATA)) { $DATA="";}
##############################################
# Process the request
#   -Post to PRS server
#   -Send back response
##############################################
if ( $WEBURL eq "" ) {
   	$WEBURL = 'http://' . $SHOST . ':' . $SPORT . $NEWURL;
}
##############
# Log Request
##############
if ($DEBUG > 0) {
	print LOG "\n*** SENDING TO PRS ***\n";
	print LOG "----> Posting Content to $WEBURL: \n";
	print LOG "flag = $DFLG \n";
	print LOG "Request METHOD: $request{METHOD}\n";
	print LOG "Request Content-type: $request{'content-type'}\n";
	print LOG "Request Accept: $request{'accept'}\n";
	print LOG "Original url: $request{URL}\n";
	print LOG "New url: $NEWURL\n";
	print LOG "$DATA\n";
}
if ( $WEBURL eq "fail" ) {
    	$PAYLOAD_LEN = 0;
	$RESP = "HTTP/1.1 404 Not Found\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\n\n";
} else {
    ##############
    # Re-post Send
    ##############
    my $userAgent = LWP::UserAgent->new;
    if ($request{METHOD} eq 'POST') {
	$resp = $userAgent->request(POST $WEBURL, 
		Content_Type => $request{'content-type'},
		Accept => $request{'accept'},
		Content => $DATA ); 
	$RESP = $resp->as_string();
    	#$PAYLOAD_LEN = $resp->content_length(); returns undefined
    } elsif ($request{METHOD} eq 'GET') {
	$resp = $userAgent->request(GET $WEBURL);
	$RESP = $resp->as_string();
    	#$PAYLOAD_LEN = $resp->content_length(); returns undefined
    } elsif ($request{METHOD} eq 'DELETE') {
        $resp = REST::Client->new;
        #$resp = $userAgent->request(DELETE $WEBURL);
        #$resp = $userAgent->delete($WEBURL);
        $resp->DELETE("$WEBURL");
        print LOG $resp->responseCode() . "\n";
        foreach ($resp->responseHeaders() ){;
          print LOG $_ . "=" . $resp->responseHeader($_) . "\n";
        }
        $RESP = $resp->responseContent();
        print LOG "$RESP\n";
        #$PAYLOAD_LEN = $resp->content_length(); returns undefined
    } else {
    	$PAYLOAD_LEN = 0;
	$RESP = "HTTP/1.1 404 Not Found\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\n\n";
    }
    if ( index($RESP,"<?xml") > -1) {
    	$PAYLOAD_LEN=length(substr($RESP, index($RESP,"<?xml")));
    } elsif ( index($RESP,"{") > -1) {
    	$PAYLOAD_LEN=length(substr($RESP, index($RESP,"{")));
    } else { 
    	$PAYLOAD_LEN = 0;
    }
}
print LOG "response content length: $PAYLOAD_LEN\n";
return ($RESP,$PAYLOAD_LEN,$tmpURL,$SubURL,$DFLG);
} # END SUB

sub do_non_prs_req {

##############################################
# Non-PRS request processing
##############################################
my $PAYLOAD_LEN = 0;
my $RESP = "";
my $CMD = "";
my $DATA = "";
my $PRG;
my $PRC;
my $XMLTYPE;
my $P2;
my $XMLDIR;
my $LAB;
my $ACCT;
my $PWD;
my $POSTFILE;
my $XMLFILE;

if ($DEBUG > 0) {
	print LOG "** Non-PRS incoming request **\n";
	print LOG "Request METHOD: $request{METHOD}\n";
	print LOG "Request Content-type: $request{'content-type'}\n";
	print LOG "Request Accept: $request{'accept'}\n";
	print LOG "Request DATA: $request{CONTENT}\n";
	print LOG "Original url: $request{URL}\n";
}
my ($REQURL,$QP) = split(/\?/, $request{URL} ,2);
my ($APPDIR,$APPCMD) = (split(/\//, $REQURL))[1,2];
print LOG "APPDIR:APPCMD:QP " . $APPDIR . ":" . $APPCMD . ":" . $QP . "\n";
##########
# Special 3PAS command list
##########
my $EVENTDIR=substr($APPDIR,0,6);
my $EVENTCMD=substr($APPCMD,0,5);
if (
  ##########
  # trivial 200 OK response
  ##########
    ( (($APPDIR eq "sample") || ($APPDIR eq "test")) && 
	(
	  ($APPCMD eq "eventlog") ||
	  ($APPCMD eq "wait4it") ||
	  ($APPCMD eq "pandc_resp") ||
	  ($APPCMD eq "pandr_resp") 
	)
    ) || 
    ( ($APPDIR eq "ford") && 
	(
	  ($APPCMD eq "ford") ||
	  ($APPCMD eq "crowd_resp")
	)
    ) || 
    ( ($APPDIR eq "audi") && 
	  ($APPCMD eq "cardata")
    ) ||
    ( ($APPDIR eq "summit") && 
	(
	  ($APPCMD eq "cardata") ||
	  ($APPCMD eq "beacon") ||
	  ($APPCMD eq "crowd_resp")
	)
    ) || 
    ( ($APPDIR eq "sndbx") && 
	(
	  ($APPCMD eq "ford") ||
	  ($APPCMD eq "crowd_resp")
	)
    ) || 
    ( (($APPDIR eq "whitep") || ($APPDIR eq "chibeacon")) && 
	(
	  ($APPCMD eq "prefs") ||
	  ($APPCMD eq "drive_state") ||
	  ($APPCMD eq "join_action") ||
	  ($APPCMD eq "admin_resp")
	)
    ) || 
    ( ($EVENTDIR eq "btteam") && 
	   ($EVENTCMD eq "resp0")
    ) || 
    ( $APPDIR eq "mycar" ) ||  #MOJIO WEB
    ( $APPDIR eq "myvehicle" ) || #3sacrowd APP
    ( $APPDIR eq "beacon" ) || #3sacrowd APP
    ( $APPDIR eq "glass" ) || #needs to be changed from paramRecText
    ( $APPDIR eq "sensordrone" ) #SENSORDRONE APP
) {
  if ($APPCMD eq "") { $APPCMD = $APPDIR };
  $DATA=`date +%s%N | rev | cut -c1-16`;
  chomp($DATA);
  $PRC=substr($DATA,0,8);
  #########
  # 200 OK Response - trivial or no payload
  #########
  $PAYLOAD_LEN = 24;
  $RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Type: application/json\nContent-Length: $PAYLOAD_LEN\n\n{\"process\":\"$PRC\"}\n\n";
  #$RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\n\n";
  #############
  # Write Payload or addl URL parm to file for prog
  #############
  open (PAYL, ">/tmp/$PRC.$APPCMD");
  if ( (!defined $request{'content-length'}) || ($request{'content-length'} == 0) ) {
	print PAYL "$request{URL}";
  } else {
	print PAYL "$request{CONTENT}";
  }
  close (PAYL);
  $CMD="$SPECPATH/$APPDIR/$APPCMD /tmp/$PRC.$APPCMD $request{URL} $QP 1>>/tmp/$APPCMD.log 2>&1";
} elsif (
  ##########
  # special call routing response
  ##########
	  ($APPCMD eq "continue") ||
	  ($APPCMD eq "endcall") ||
	  ($APPCMD eq "route")
) {
  #########
  # EndCall, Continue or Route Response
  #########
  $PAYLOAD_LEN = 13;
  if ( $APPCMD eq "continue" ) {
  	$RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Type: application/json\nContent-Length: $PAYLOAD_LEN\n\n{\"action\": {\"actionToPerform\": \"Continue\"}}";
  } elsif ( $APPCMD eq "endcall" ) {
  	$RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Type: application/json\nContent-Length: $PAYLOAD_LEN\n\n{\"action\": {\"actionToPerform\": \"EndCall\"}}";
  } else {
	my $ROUTE=uri_unescape(substr($QP, index($QP,"=")+1));
  	$RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Type: application/json\nContent-Length: $PAYLOAD_LEN\n\n{\"action\": {\"actionToPerform\": \"Route\", \"routingAddress\": \"sip:+1$ROUTE\"}}";
  }
} elsif (
  ##########
  # Defer response
  ##########
    ( ($APPDIR eq "ford") && 
	( ( $APPCMD eq "iAlone" ) ||
	  ( $APPCMD eq "play_fwd" ) )
    ) || 
    ( ($APPDIR eq "audi") && 
	  ( $APPCMD eq "play_fwd" )
    ) || 
    ( ($APPDIR eq "summit") && 
	( ( $APPCMD eq "iAlone" ) ||
	  ( $APPCMD eq "play_fwd" ) )
    ) || 
    ( ($APPDIR eq "sndbx") && 
	( ( $APPCMD eq "iAlone" ) ||
	  ( $APPCMD eq "play_fwd" ) )
    ) || 
    ( (($APPDIR eq "whitep") || ($APPDIR eq "chibeacon")) && 
	( ( $APPCMD eq "call_action" ) || 
	  ( $APPCMD eq "other_action" ) )
    ) || 
    ( ($APPDIR eq "test") &&
	( $APPCMD eq "defer" )
    ) || 
    ( ($EVENTDIR eq "btteam") && 
	( $EVENTCMD eq "defer" )
    ) || 
    ( ( $APPDIR eq "sample" ) && 
	( ( $APPCMD eq "pandc" ) ||
	  ( $APPCMD eq "pandr" ) )
    )
) {
  #########
  # Defer Response
  #########
  if ($APPCMD eq "") { $APPCMD = $APPDIR };
  $DATA=`date +%s%N | rev | cut -c1-16`;
  chomp($DATA);
  $PRC=substr($DATA,0,8);
  $PAYLOAD_LEN = 77;
  $RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"action\": {\"decisionId\": \"" . $DATA . "\", \"actionToPerform\": \"Deferred\"}}\n\n";
  #############
  # Write Payload or addl URL parm to file for prog
  #############
  open (PAYL, ">/tmp/$PRC.$APPCMD");
  if ( (!defined $request{'content-length'}) || ($request{'content-length'} == 0) ) {
	print PAYL "$request{URL}";
  } else {
	print PAYL "$request{CONTENT}";
  }
  close (PAYL);
  $CMD="$SPECPATH/$APPDIR/$APPCMD /tmp/$PRC.$APPCMD $DATA \"$QP\" 1>>/tmp/$APPCMD.log 2>&1";
} elsif (
    ( ($APPDIR eq "audi") && 
	  ( $APPCMD eq "getsub" )
    ) || 
    ( ($APPDIR eq "nyny") && 
	  ( $APPCMD eq "imsdata" )
    ) || 
    ( ($APPDIR eq "orchestrate") &&
	( $APPCMD eq "call_apps" )
    ) || 
    ( ($APPDIR eq "callbox") && 
          ( $APPCMD eq "req_swbd" ) ||
	  ( $APPCMD eq "rt_dest" )
    ) || 
    ( ($APPDIR eq "call") && 
	  (
	    ( $APPCMD eq "getcallid" ) ||
	    ( $APPCMD eq "find" )
	  )
    ) 
) {
  #################
  # calculate response
  #################
  $CMD="";
  $DATA=`date +%s%N | rev | cut -c1-16`;
  chomp($DATA);
  $PRC=substr($DATA,0,8);
  $PAYLOAD_LEN = 77;
  #############
  # Write Payload or addl URL parm to file for prog
  #############
  open (PAYL, ">/tmp/$PRC.$APPCMD");
  if ( (!defined $request{'content-length'}) || ($request{'content-length'} == 0) ) {
	print PAYL "$request{URL}";
  } else {
	print PAYL "$request{CONTENT}";
  }
  close (PAYL);
  my $RSLT=`$SPECPATH/$APPDIR/$APPCMD /tmp/$PRC.$APPCMD \"$QP\" 2>/tmp/$APPCMD.log`;
  print LOG "RSLT:" . $RSLT . "\n";
  my ($CODE,$_) = split(/\,/, $RSLT ,2);
  if ( index($CODE,"200")  > -1) {
    if ( $APPDIR eq "audi") {
      my ($_,$VIN,$MBNUM,$CARNUM,$NAME,$GOOG,$LAB) = split(/\,/, $RSLT ,7);
      chomp($LAB);
      $RESP = "HTTP/1.1 $CODE\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"data\": {\"vin\": \"" . $VIN . "\",\"mobile_num\": \"" . $MBNUM . "\",\"vehicle_num\": \"" . $CARNUM . "\",\"driver_name\": \"" . $NAME . "\",\"google_ref\": \"" . $GOOG . "\",\"lab\": \"" . $LAB . "\"}}\n\n";
    } elsif ( $APPDIR eq "nyny") {
      my ($_,$SIM,$ACCT,$CCSLOGIN,$CCSPWD) = split(/\,/, $RSLT ,5);
      chomp($CCSPWD);
      $RESP = "HTTP/1.1 $CODE\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"data\": {\"sim_number\": \"" . $SIM . "\", \"sip_number\": \"" . $ACCT . "\", \"ccs_login\": \"" . $CCSLOGIN . "\", \"ccs_pwd\": \"" . $CCSPWD . "\"}}\n\n";
    } elsif ( ($APPDIR eq "callbox") && ( $APPCMD eq "req_swbd" ) ) {
      my ($_,$ACTION,$ATTND,$DECID) = split(/\,/, $RSLT ,4);
      chomp($DECID);
      $RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"action\": {\"actionToPerform\": \"$ACTION\", \"extensionAddress\": \"sip:$ATTND\"}}\n\n";
    } elsif ( ($APPDIR eq "callbox") && ( $APPCMD eq "rt_dest" ) ) {
      my ($_,$ACTION,$DECID,$ROUTE) = split(/\,/, $RSLT ,4);
      chomp($ROUTE);
      if ( $DECID ne "NA") {
        $RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"action\": {\"decisionId\": \"" . $DECID . "\", \"actionToPerform\": \"$ACTION\"}}\n\n";
      } else {
        $RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"action\": {\"actionToPerform\": \"$ACTION\", \"routingAddress\": \"sip:$ROUTE\"}}\n\n";
      }
    } elsif ( $APPDIR eq "call") {
      my ($_,$CALLID,$TS) = split(/\,/, $RSLT ,3);
      chomp($TS);
      $RESP = "HTTP/1.1 $CODE\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"data\": {\"callid\": \"" . $CALLID . "\",\"timestamp\": \"" . $TS . "\"}}\n\n";
    } elsif ( $APPDIR eq "orchestrate") {
      my ($_,$ATP,$DECID) = split(/\,/, $RSLT ,3);
      chomp($DECID);
      if ( $DECID ne "NA'") {
        $RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"action\": {\"decisionId\": \"" . $DECID . "\", \"actionToPerform\": \"Deferred\"}}\n\n";
      } else {
        $RESP = "HTTP/1.1 200 OK\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"action\": {\"actionToPerform\": \"" . $ATP . "\"}}\n\n";
      }
    }
  } else {
    my ($_,$EC) = split(/\,/, $RSLT ,2);
    chomp($EC);
    $RESP = "HTTP/1.1 $CODE\nConnection: close\nConnection: close\nContent-Length: $PAYLOAD_LEN\nContent-Type: application/json\n\n{\"error\": \"" . $EC . "\"}\n\n";
  }
} else {
  #################
  # fail Response #
  #################
  $RESP = "HTTP/1.1 404 Not Found\nConnection: close\nConnection: close\nContent-Length: 0\n\n";
}
return ($RESP,$PAYLOAD_LEN,$CMD);
} #END SUB

sub fixup_response{

my $TYPE = $_[0];
my $RESP = $_[1];
my $OPAYLOAD_LEN = $_[2];
my $DFLG = $_[3];
my $PAYLOAD;
my $PAYLOAD_LEN;
my $RESPHD;

if ( $TYPE eq "xml" ) {
    	$RESPHD=substr($RESP, 0, index($RESP,"<?xml") );
    	$PAYLOAD=substr($RESP, index($RESP,"<?xml"));
} elsif ( $TYPE eq "json" ) {
    	$RESPHD=substr($RESP, 0, index($RESP,"{") );
    	$PAYLOAD=substr($RESP, index($RESP,"{"));
} else { 
	$RESPHD = $RESP;
	$PAYLOAD = "";
}
print LOG "RESPONSE: " . $RESP . "\n";
print LOG "length RESPONSE: " . length($RESP) . "\n";
print LOG "orig length RESPHD: " . length($RESPHD) . "\n";
print LOG "orig length PAYLOAD: " . $OPAYLOAD_LEN . "\n";
#$RESPHD =~ s/Client.*\012//g;
$RESPHD =~ s/\012/\015\012/g;
$PAYLOAD_LEN = length($PAYLOAD);
if (( $TYPE eq "xml" ) || ( $TYPE eq "json" )) {
    if (($DFLG eq "9to10") || ($DFLG eq "sub9to10")) {
	$PAYLOAD = &manipulate_post("data",$PAYLOAD,$OTSTR,$INSTR);
	#$PAYLOAD = &manipulate_post("data",$PAYLOAD,"REST","REST/1.0");
	#$PAYLOAD = &manipulate_post("data",$PAYLOAD,".14:8080",".16:8080");
	#$PAYLOAD = &manipulate_post("data",$PAYLOAD,"/v1/","/");
    } elsif ($DFLG eq "urlout") {
	# FIX Response back to PRS into PRS10 format
	$PAYLOAD = &manipulate_post("data",$PAYLOAD,$INSTR,$OTSTR);
    #} else {
    #	$RESPHD =~ s/\012/\015\012/g;
    }
}
$PAYLOAD_LEN = length($PAYLOAD);
if ($OPAYLOAD_LEN != $PAYLOAD_LEN) {
    $RESPHD = &manipulate_post("data",$RESPHD,"Content-Length: " . $OPAYLOAD_LEN,"Content-Length: " . $PAYLOAD_LEN);
} 
my $FRESP = $RESPHD . $PAYLOAD;
print LOG "new length PAYLOAD: " . $PAYLOAD_LEN . "\n";
print LOG "new length RESPHD: " . length($RESPHD) . "\n";
print LOG "new length RESP: " . length($FRESP) . "\n";

return $FRESP;
} # END SUB

sub manipulate_post {
	#############################
	# Custom config for this application
	# Usage manipulate_post(indata, extstr, chgstr)
	#############################
	my $new_data;
	if ($_[0] ne "urlout") {
		$new_data = $_[1];
		$new_data =~ s/$_[2]/$_[3]/g;
	} else {
		# find 3rd prty app server url
		$new_data = &find_url($_[1]);
		#if ($new_data eq "not_found") { 
		#	$new_data = $_[1];
		#}
	}
   	if ($DEBUG > 1) {
		print LOG "----> parm1 $_[0] \n";
		print LOG "----> parm2 $new_data \n";
		print LOG "----> parm3 $_[2] \n";
		print LOG "----> parm4 $_[3] \n";
		print LOG "----> $new_data \n";
	}
	$new_data;
}
#############################
# find the correct outbound app server url
#############################
sub find_url
{
	my $myurl1;
	my @fnd = &grepit($_[0],$URLFILE,"f");
	my $cnt=@fnd;
	if ($cnt == 0) { $myurl1="not_found"; }
	else { 
		($_, $myurl1, $_) = split(/;/,$fnd[0],3);
		chomp($myurl1);
	}
   	if ($DEBUG > 1) {
		print LOG "fnd cnt is $cnt \n";
		print LOG "new url1: $myurl1 \n";
	}
	$myurl1;
}
#########################################################
# subroutine for grep
#########################################################
sub grepit {

my $SEEK = $_[0];
my $FILE = $_[1];
my $FLG = $_[2];
my @found;
my @notfound;
#
#open(SUBGREP, "-|") || exec "/usr/bin/grep", $MB, $SUBFILE);
#
open(GREP, $FILE);
my @lines=<GREP>;
close(GREP);
if ($DEBUG > 1) {print LOG "seeking $SEEK \n"; }
foreach $_ (@lines) {
	if ($DEBUG > 1) {print LOG "in line: $_ \n"; }
	if (/$SEEK/)
	{
		if ($DEBUG > 1) {print LOG "fnd\n"; }
		push(@found,$_);
	} else {
		if ($DEBUG > 1) {print LOG "notfnd\n"; }
		push(@notfound,$_);
	}
}
if ($FLG eq "not") {
	@notfound;
} else {
	@found;
}

}

