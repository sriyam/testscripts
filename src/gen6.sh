DIR=$HOME
# LABS
FOAWS=172.22.43.39
#
SRCIP=172.22.23.173
SRCPORT=8085
DSTIP=$FOAWS
DSTPORT=6565
SPAI=NA
SEND_ID=0
if
  test "${1}" = ""
then
  TEST=""
else
  #case ${1} in
  #  INFORM*)
  #    TEST=`grep ^${1}: inform_tests.data`
  #    ;;
  #  *)
  #    TEST=`grep ^${1}: tests.data`
  #    ;;
  #esac
  TEST=`grep ^${1}: $DIR/tests.data`
fi
if
  test "$TEST" = ""
then
  echo "Usage: gen2.sh scam001 [TYPE (xml payload)] - to send an INVITE to Delegator"
  echo "valid test cases are: "
  tail -n+2 *.data | cut -f1 -d":" 
  exit
fi
TESTNUM=`echo $TEST | cut -f1 -d":"`
A=`echo $TEST | cut -f2 -d":"`
B=`echo $TEST | cut -f3 -d":"`
PAI=`echo $TEST | cut -f4 -d":"`
PI=`echo $TEST | cut -f5 -d":"`
if
  test "$2" != ""
then
  case $2 in
    send_id)
        SEND_ID=1
        ;;
    *.xml)
        TYPE=$2
        ;;
    *ONLY)
        LIMITER=$2
        ;;
    new)
        SERVER=17
        ;;
    *)
        echo "Unexpected parameter 2 - quitting"
        exit
        ;;
  esac
fi
C4=`echo $TESTNUM | cut -c1-4`
case $C4 in
  mpnb | rest)
    case $PI in
      sip) URI=sip
           ;;
      priv) URI=priv
           ;;
      mailto) URI=mailto
           ;;
      tel) URI=tel
           ;;
      *) 
           ;;
    esac
    PID=`$DIR/rest${SERVER}.sh $C4 $A $B $URI $PAI`
    ;;
  *) if
     test "$TYPE" = ""
    then
    case $PAI in
      "") case $PI in
        PR) TYPE=emer.xml
	   ;;
        PI) TYPE=pi.xml
	   ;;
	VRP) TYPE=verstat_passed.xml
	   ;;
	VRN) TYPE=verstat_no.xml
	   ;;
	VRF) TYPE=verstat_failed.xml
	   ;;
	INF) TYPE=special_inform.xml
	   ;;
        xmav) TYPE=xmav.xml
	   ;;
        noplus) TYPE=noplus.xml
	   ;;
	*) TYPE=base.xml
	   ;;
        esac
        ;;
      PAI) case $PI in
        PR) TYPE=pai_emer.xml
	   ;;
        PI) TYPE=pai_pi.xml
	   ;;
	ANON) TYPE=anon_with_pai.xml
	   ;;
	ANONCOM) TYPE=anon_comcast.xml
	   ;;
	VRP) TYPE=pai_verstat_passed.xml
	   ;;
	VRN) TYPE=pai_verstat_no.xml
	   ;;
	VRF) TYPE=pai_verstat_failed.xml
	   ;;
        nopluspai) TYPE=nopluspai.xml
	   ;;
	VRP+PI) TYPE=pai_verstat_passed_pi.xml
	   ;;
        custom1) TYPE=verstat_custom1.xml
	   ;;
        custom2) TYPE=verstat_custom2.xml
	   ;;
	*) TYPE=pai.xml
	   ;;
        esac
        ;;
      2PAI) case $PI in
        PI) TYPE=2pai_pi.xml
	   ;;
         *) TYPE=2pai.xml
	   ;;
        esac
        ;;
      *) # PAI and A do not match
        SPAI=$PAI
        case $PI in
          PR) TYPE=pai_emer.xml
	   ;;
          PI) TYPE=pai_pi.xml
	   ;;
          TO) TYPE=diff_to.xml
	   ;;
	  *) TYPE=diff_pai.xml
	   ;;
        esac
        ;;
    esac
    fi

    if
       test "$C4" = "stir"
    then
      #echo "XML file is $TYPE"
      cp $DIR/xml/$TYPE $DIR/xml/sti-as.xml
      VERSTAT_EXT=`echo $TYPE | grep verstat`
      if
	test "$VERSTAT_EXT" = ""
      then
        sed -i '/From:/ s/user=phone/user=phone;verstat=TN-Validation-Passed/' $DIR/xml/sti-as.xml
        sed -i '/P-Asserted-Identity:/ s/user=phone/user=phone;verstat=TN-Validation-Passed/' $DIR/xml/sti-as.xml
      fi
      XMLF=sti-as.xml
    elif 
      test $SEND_ID -eq 1
    then
        cp $DIR/xml/verstat_passed.xml $DIR/xml/sti-as.xml
        LIMITER=ASONLY
        XMLF=sti-as.xml
    else
      XMLF=$TYPE
    fi
    #echo "FINAL XML file is $XMLF"
    echo calling delegator $XMLF $LIMITER
    PID=`$DIR/call_delegator.sh $SRCIP $SRCPORT $DSTIP $DSTPORT $A $B $XMLF $SPAI $LIMITER`
    ;;
esac
if
   test $SEND_ID -eq 0
then
  mv /tmp/$PID.log $DIR/logs/$TESTNUM.log
  cat $DIR/logs/$TESTNUM.log
else
  mv /tmp/$PID.log $DIR/logs/stir001.log
  XMLF=${TESTNUM}_withID.xml
  cp $DIR/xml/$TYPE $DIR/xml/$XMLF
  $DIR/get_id.sh $DIR/xml/$XMLF
  echo calling delegator $XMLF VSONLY
  PID=`$DIR/call_delegator.sh $SRCIP $SRCPORT $DSTIP $DSTPORT $A $B $XMLF $SPAI VSONLY`
  mv /tmp/$PID.log $DIR/logs/$TESTNUM.log
  cat $DIR/logs/$TESTNUM.log
fi
