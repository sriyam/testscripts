DT=`date +%Y%m%d-%H%M`
EPOCH=`date +%s`
#echo $DT
#echo $EPOCH
SIGNREQ=/tmp/$$.signreq
SIGNRESP=/tmp/$$.signresp
VERREQ=/tmp/$$.verreq
VERRESP=/tmp/$$.verresp
echo AUTHENTICATION
cat ./signReq.json | sed -e "s/DATE/$EPOCH/" > /tmp/$$.signReq.json
#
# AUTHENTICATION
#
echo AS1 - InstanceID, and RequestID
#
mv /tmp/$$.signReq.json ${SIGNREQ}
curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" -H "X-RequestID: ${DT}-abcd-${EPOCH}" -H "X-InstanceID: ${EPOCH}-abcd-${DT}" http://172.22.43.234:8080/stir/v1/signing -d @${SIGNREQ} > ${SIGNRESP}
#
#echo AS2 - no RequestID
#
#mv /tmp/$$.signReq.json ${SIGNREQ}
#curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" -H "X-InstanceID: ${EPOCH}-abcd-${DT}" http://172.22.43.234:8080/stir/v1/signing -d @${SIGNREQ} > ${SIGNRESP}
#
#echo AS3 - no InstanceID, no RequestID
#
#mv /tmp/$$.signReq.json ${SIGNREQ}
#curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" http://172.22.43.234:8080/stir/v1/signing -d @${SIGNREQ} > ${SIGNRESP}
#
echo AS4 - wrong iat
#
#cat /tmp/$$.signReq.json | sed -e "s/${EPOCH}/12345/" > /tmp/$$.signReq2.json
#mv /tmp/$$.signReq2.json ${SIGNREQ}
#curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" -H "X-RequestID: ${DT}-abcd-${EPOCH}" -H "X-InstanceID: ${EPOCH}-abcd-${DT}" http://172.22.43.234:8080/stir/v1/signing -d @${SIGNREQ} > ${SIGNRESP}
#
#echo AS5 - bad payload
#
#cat /tmp/$$.signReq.json | sed -e "s/attest/att/" > /tmp/$$.signReq2.json
#mv /tmp/$$.signReq2.json ${SIGNREQ}
#curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" -H "X-RequestID: ${DT}-abcd-${EPOCH}" -H "X-InstanceID: ${EPOCH}-abcd-${DT}" http://172.22.43.234:8080/stir/v1/signing -d @${SIGNREQ} > ${SIGNRESP}
#
# print request
#
echo input payload
cat ${SIGNREQ}
#
# print response
#
cat ${SIGNRESP}
echo
echo
#
# VERIFICATION
echo VERIFICATION
#
#
cat ./verfReq.json | sed -e "s/DATE/$EPOCH/" > ${VERREQ}
grep identity ${SIGNRESP} >> ${VERREQ}       # ID from AS
#grep identity ${SIGNRESP} | sed -e 's/\\\"shaken\\\"/shaken/' >> ${VERREQ}       # remove \" on shaken
echo "}}" >> ${VERREQ}
#
#echo VS1 - no InstanceID, and no RequestID
#
#curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" http://172.22.43.234:8080/stir/v1/verification -d @${VERREQ} > ${VERRESP}
#
#echo VS2 - InstanceID, no RequestID
#
curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" -H "X-InstanceID: ${EPOCH}-dcba-${DT}" http://172.22.43.234:8080/stir/v1/verification -d @${VERREQ} > ${VERRESP}
#
#echo VS3 - InstanceID, and RequestID
#
#curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" -H "X-RequestID: ${DT}-dcba-${EPOCH}" -H "X-InstanceID: ${EPOCH}-dcba-${DT}" http://172.22.43.234:8080/stir/v1/verification -d @${VERREQ} > ${VERRESP}
#
#echo VS4 - bad payload
#
#cat ${VERREQ} | sed -e "s/star/str/" > /tmp/$$.badverf
#mv /tmp/$$.badverf ${VERREQ}
#curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" -H "X-RequestID: ${DT}-dcba-${EPOCH}" -H "X-InstanceID: ${EPOCH}-dcba-${DT}" http://172.22.43.234:8080/stir/v1/verification -d @${VERREQ} > ${VERRESP}
#
#echo VS5 - missing parameter
#
sed '/to/,+2d' < ${VERREQ} > /tmp/$$.badverf
mv /tmp/$$.badverf ${VERREQ}
curl -Ssik --connect-timeout 5 -H "Accept: application/json" -H "Content-Type: application/json" -H "X-RequestID: ${DT}-dcba-${EPOCH}" -H "X-InstanceID: ${EPOCH}-dcba-${DT}" http://172.22.43.234:8080/stir/v1/verification -d @${VERREQ} > ${VERRESP}
#
echo input payload
cat ${VERREQ}
#
# print response
#
cat ${VERRESP}
echo
#rm -f /tmp/$$.*
echo
echo
