TEST=${1}
P1=$2
VERBOSE=${3}
if
  test "${TEST}" = ""
then
  echo "need a test#"
  exit
fi
if
  test "$VERBOSE" = "v"
then
  $HOME/gen2.sh $TEST $P1
else
  $HOME/gen2.sh $TEST $P1 1>/dev/null 
fi
$HOME/check_test.sh $TEST
