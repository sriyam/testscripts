DIR=$HOME
LABCONFIGF=$DIR/labconfig.txt
ORIGTERM=TERM
SPAI=NA
SEND_ID=0
#FUNCTIONS
function set_ip_ports()
{
  local LABCONFIG=$1
  local LAB=$2
  local USER=$3
  local SERVTYPE=$4
  CONFIG=`grep ^$LAB:$USER:$SERVTYPE $LABCONFIG`
  if
    test "$CONFIG" != ""
  then
    SRCIP=`echo $CONFIG | cut -f4 -d":"` 
    SRCPORT=`echo $CONFIG | cut -f5 -d":"`
    DSTIP=`echo $CONFIG | cut -f6 -d":"`
    DSTPORT=`echo $CONFIG | cut -f7 -d":"`
    RC=0
  else
    RC=2
  fi
  return $RC
}

function sendINV_to_delegator()
{
  local LAB=$1
  local USER=$2
  local SERVTYPE=$3
  local XML=$4
  local OUTFILE=$5
  local RC
  set_ip_ports $LABCONFIGF $LAB $USER $SERVTYPE
  RC=$?
  if
    test $RC -eq 0
  then
    if
      test "$PAI" = ""
    then
      $DIR/SippCaller.py -a $ANUM -b $BNUM -s $SRCIP --srcport $SRCPORT -d $DSTIP --dstport $DSTPORT --notls -x $DIR/xml/$XML 1>$OUTFILE
    else
      if
        test "$PAI" = "PAI"
      then
        SENDPAI=$ANUM
      else
        SENDPAI=$PAI
      fi
      $DIR/SippCaller.py -a $ANUM --pai $SENDPAI -b $BNUM -s $SRCIP --srcport $SRCPORT -d $DSTIP --dstport $DSTPORT --notls -x $DIR/xml/$XML 1>$OUTFILE
    fi
  fi
  echo $RC
}

function set_xmlfile ()
{
  local TESTIS=$1
  local INCL_PAI=`echo $2 | cut -f2 -d":"`
  local SPECIAL_XML=$3 #OPTIONAL Parameter
  local TYPE
  #echo PAI:$INCL_PAI
  #echo SPEC:$2
  # FIND THE XML FILE
  case $INCL_PAI in
    "") 
      case $SPECIAL_XML in
        PR) TYPE=emer.xml
	   ;;
        PI) TYPE=pi.xml
	   ;;
	VRP) TYPE=verstat_passed.xml
	   ;;
	VRPD) TYPE=vrp_date.xml
	   ;;
	VRN) TYPE=verstat_no.xml
	   ;;
	VRF) TYPE=verstat_failed.xml
	   ;;
        hih_div*) TYPE=hih_div.xml
		TYPE=$(fix_xml_file $SPECIAL_XML $TYPE)
	   ;;
        hih*) TYPE=hih.xml
		TYPE=$(fix_xml_file $SPECIAL_XML $TYPE)
	   ;;
        div*) TYPE=div.xml
		TYPE=$(fix_xml_file $SPECIAL_XML $TYPE)
	   ;;
	rphe) TYPE=rphe.xml
	   ;;
	rphw) TYPE=rphw.xml
	   ;;
	rph) TYPE=rph.xml
	   ;;
	attestInfo) TYPE=attest-info.xml
	   ;;
        xmav) TYPE=xmav.xml
	   ;;
        xmavCobalt) TYPE=xmavCobalt.xml
	   ;;
        noplus) TYPE=noplus.xml
	   ;;
        noplusto) TYPE=noplusto.xml
	   ;;
        custom1) 
           TYPE=sti-as2.xml
	   ;;
        custom2) 
           TYPE=sti-as3.xml
	   ;;
        vcustom1) 
           TYPE=verstat_custom1.xml
	   ;;
        nobracket)
           TYPE=nobracket.xml
	   ;;
        nobracphone)
           TYPE=nobracphone.xml
	   ;;
        DATE)
           TYPE=date.xml
	   ;;
        base2)
           TYPE=base2.xml
	   ;;
	*) TYPE=base.xml
	   ;;
      esac
      ;;
    PAI) 
      case $SPECIAL_XML in
        PR) TYPE=pai_emer.xml
	   ;;
        PI) TYPE=pai_pi.xml
	   ;;
	ANON) TYPE=anon_with_pai.xml
	   ;;
	ANONCOM) TYPE=anon_comcast.xml
	   ;;
	VRP) TYPE=pai_verstat_passed.xml
	   ;;
	VRN) TYPE=pai_verstat_no.xml
	   ;;
	VRF) TYPE=pai_verstat_failed.xml
	   ;;
        nopluspai) TYPE=nopluspai.xml
	   ;;
        noplusboth) TYPE=noplusboth.xml
	   ;;
        noplusfrom) TYPE=noplusfrom.xml
	   ;;
        noplusto) TYPE=noplusto.xml
	   ;;
	VRP+PI) TYPE=pai_verstat_passed_pi.xml
	   ;;
        custom1) 
           TYPE=sti-as2.xml
	   ;;
        custom2) 
           TYPE=sti-as3.xml
	   ;;
        vcustom1) 
           TYPE=verstat_custom1.xml
	   ;;
   	double_plus) 
           TYPE=double_plus.xml
	   ;;
   	siptel) 
           TYPE=tmo_pai.xml
	   ;;
	rphe) TYPE=rphe_pai.xml
	   ;;
	rphw) TYPE=rphw_pai.xml
	   ;;
	rph) TYPE=rph_pai.xml
	   ;;
        xmavapp) TYPE=xmavapp.xml
	   ;;
        hih_div*) TYPE=hih_div.xml
		TYPE=$(fix_xml_file $SPECIAL_XML $TYPE)
	   ;;
        hih*) TYPE=hih.xml
		TYPE=$(fix_xml_file $SPECIAL_XML $TYPE)
	   ;;
        div*) TYPE=div.xml
		TYPE=$(fix_xml_file $SPECIAL_XML $TYPE)
	   ;;
	*) TYPE=pai.xml
	   ;;
      esac
      ;;
    2PAI)
      case $SPECIAL_XML in
        PI) TYPE=2pai_pi.xml
	   ;;
         AS) TYPE=2paiAS.xml
           ;;
         V) TYPE=2pai_verstat.xml
           ;;
         *) TYPE=2pai.xml
	   ;;
      esac
      ;;
    2TEL)
      case $SPECIAL_XML in
        V) TYPE=2tel_verstat.xml
           ;;
        *) TYPE=2tel.xml
	   ;;
      esac
      ;;
    *) # PAI and A do not match
      SPAI=$INCL_PAI
      case $SPECIAL_XML in
        PR) TYPE=pai_emer.xml
	   ;;
        PI) TYPE=pai_pi.xml
	   ;;
        TO) TYPE=diff_to.xml
	   ;;
        nopluspai) TYPE=nopluspai.xml
	   ;;
        noplusboth) TYPE=noplusboth.xml
           ;;
        noplusfrom) TYPE=noplusfrom.xml
	   ;;
	 *) TYPE=diff_pai.xml
	   ;;
      esac
      ;;
  esac
  case $TESTIS in
     stir*vs)
         ;;
     stir*)
           # THIS IS STI-AS CALLS
           #echo $TYPE
           TYPE=$(add_verstat_to_xml $TYPE)
        ;;
     *)
        ;;
  esac
  echo $TYPE
}

fix_xml_file()
{
  local XMLPARMS=$1
  local FILE=$2
  local PARM2
  local OUTXML=hih_$$_temp.xml
  
  PARM2=`echo $XMLPARMS | cut -s -f2 -d";"`
  if
    test "$PARM2" != ""
  then
    cat $DIR/xml/$FILE | sed -e "s/NNNNNNNN/$PARM2/" > $DIR/xml/$OUTXML
  else
    cp $DIR/xml/$FILE $DIR/xml/$OUTXML
  fi
  echo $OUTXML
}

function add_verstat_to_xml ()
{
  local INFILE=$1
  local VERSTATF=sti-as.xml
  FNDV=`grep -c "verstat=" $DIR/xml/$INFILE`
  FNDA=`grep -c "Attestation-Info:" $DIR/xml/$INFILE`
  FNDP=`grep -c "P-Attestation-Indicator:" $DIR/xml/$INFILE`
  if
    test $FNDV -eq 0 -a $FNDA -eq 0 -a $FNDP -eq 0
  then
    cat $DIR/xml/$INFILE | \
      sed -e '/From:/ s/user=phone/user=phone;verstat=TN-Validation-Passed/' | \
      sed -e '/From: <tel/ s/>/;verstat=TN-Validation-Passed>/' | \
      sed -e '/P-Asserted-Identity:.*<sip/ s/user=phone/user=phone;verstat=TN-Validation-Passed/' | \
      sed -e '/P-Asserted-Identity: <tel/ s/>/;verstat=TN-Validation-Passed>/' > $DIR/xml/$VERSTATF
  else
    cat $DIR/xml/$INFILE > $DIR/xml/$VERSTATF
  fi
  echo $VERSTATF
}
function chg_base_curl_xml () 
{
  local INFILE=$1
  local IP=$2
  local A=$3
  local B=$4
  local URITYPE=$5
  local OTHERCHGS=`echo $6 | cut -f2- -d":"`
  local OUTFILE=/tmp/$$.curl
  
  case $URITYPE in
    *sip)
      URI1=sip:+
      URI2=sip:+
      ;;
    *priv)
      URI1=priv:+
      URI2=tel:+
      ;;
    *mailto)
      URI1=mailto:
      URI2=tel:+
      ;;
    *)
      URI1=tel:+
      URI2=tel:+
      ;;
  esac
  DT=`date +%s`
  cat $DIR/xml/${XML}.curl | \
    sed -e "s/SERVER/${IP}/" | \
    sed -e "s/DT/${DT}/" | \
    sed -e "s/AAAAAA/${A}/" | \
    sed -e "s/BBBBBB/${B}/" | \
    sed -e "s/URI1/${URI1}/g" | \
    sed -e "s/URI2/${URI2}/g" > $OUTFILE
  if
    test "$OTHERCHGS" != ""
  then 
    P1=`echo $OTHERCHGS | cut -f1 -d"-"`
    P2=`echo $OTHERCHGS | cut -f2 -d"-"`
    cat $OUTFILE | sed -e "s/$P1/$P2/" > /tmp/$$.tmpxml
    mv /tmp/$$.tmpxml $OUTFILE
  fi
  chmod 755 $OUTFILE
  echo $OUTFILE
}

function send_curl_to_edge ()
{
  local SERVERIP=$1
  local XMLTYPE=$2
  local A=$3
  local B=$4
  local URIS=$5
  local SPECIAL_CHGS=$6
  local XML
  local URI1
  local URI2
  case $XMLTYPE in
    rest)
        XML=vrest
        ;;
    mpnb)
        XML=mrest
       ;;
      *) echo 9999
       exit
       ;;
  esac
  XMLFILE=$(chg_base_curl_xml $XML $SERVERIP $A $B $URIS $SPECIAL_CHGS)
  if
   test -r $XMLFILE
  then
    cp $XMLFILE $LOGFILE
    $XMLFILE >> $LOGFILE
    rm $XMLFILE
    echo "0"
  else
    echo "666"
  fi
}

function get_sipp_log()
{
  #retrive log and cleanup
  SIPPOUTF=$1
  LOGFILE=/tmp/$$.log
  OUTDIR=`tail -1 $SIPPOUTF | cut -f3 -d" "`
  if
    test -r $OUTDIR
  then
    cp $OUTDIR/*.log $LOGFILE
    rm -rf $OUTDIR
  else
    mv $SIPPOUTF $LOGFILE
  fi
  echo $LOGFILE
}

function build_term_xmlfile ()
{
  ASLOG=${1}
  VSXMLF=${2}
  local TMPINVF=/tmp/$$.tmpinv
  local BLD=0
  local LC=0
  #cp $ASLOG /tmp/$$.dfkinv
  if
    test ! -f $ASLOG
  then
    # "Error: no output file from which to get Identity"
    BLD=15
  else
    STRESPL=`grep -n -m1 "302 Moved Temporarily" ${ASLOG} | cut -f1 -d":"`
    ENDRESPL=`grep -n -m1 "Content-Length: 0" ${ASLOG} | cut -f1 -d":"`
    if
      test "${STRESPL}" = "" -o "${ENDRESPL}" = "" 
    then
      # "Error: No response not as expected"
      BLD=20
    else
      #GET Identity from AS LOG
      HDR=`tail -n +${STRESPL} $ASLOG | head -n $(($ENDRESPL - $STRESPL + 1)) | grep "^Identity:" | tr -d '\r'`
      echo $HDR | sed -e "s/ Identity:/\nIdentity:/g" > /tmp/$$.idfile
      LC=`cat /tmp/$$.idfile | wc -l`
      if
        test $LC -eq 0
      then
        # "NO IDENTITY found in AS logfile: $ASLOG"
        BLD=25
      fi
    fi
  fi
  if
    test $BLD -eq 0
  then
    CSEQL=`grep -m 1 -n -i CSeq $VSXMLF | cut -f1 -d":"`
    N=`expr $CSEQL \- 1`
    head -$N $VSXMLF > $TMPINVF
    cat /tmp/$$.idfile >> $TMPINVF
    #echo $HDR >> $TMPINVF
    tail -n +$CSEQL $VSXMLF >> $TMPINVF
    mv $TMPINVF $VSXMLF
  fi
  echo $BLD
}
#
### START NXTGEN
#
if
  test "${1}" = ""
then
  TEST=""
else
  TEST=`grep ^${1}: $DIR/911.data`
fi
if
  test "$TEST" = ""
then
  echo "Usage: nxtgen.sh scam001 [LAB (xml payload)]"
  echo "not a valid test case  - see 911.data"
  exit
fi
TESTNUM=`echo $TEST | cut -f1 -d":"`
rm -f $DIR/logs/$TESTNUM.log
ANUM=`echo $TEST | cut -f2 -d":"`
BNUM=`echo $TEST | cut -f3 -d":"`
PAI=`echo $TEST | cut -f4 -d":"`
SPECIAL=`echo $TEST | cut -f5 -d":"`
case $TESTNUM in
    stir*vs)
      SEND_ID=1
      ORIGTERM=TERM
      ;;
    stir*)
      ORIGTERM=ORIG
      ;;
    orig*)
      ORIGTERM=ORIG
      ;;
    orch*s)
      SEND_ID=1
      ORIGTERM=TERM
      ;;
    emer*)
      ORIGTERM=ORIG
      ;;
     *)
      ORIGTERM=TERM
      ;;
esac
USER=`echo $HOME| cut -f3 -d"/"`
if
  test "${2}" = ""
then
  case $USER in
    dkliebhan)
      EDGE=180
      ;;
    smishra1)
      EDGE=39
      ;;
    *)
      echo "WHO ARE YOU"
      exit
      ;;
  esac
else
  EDGE=$2
fi
if
  test "$3" != ""
then
  case $3 in
    send_id)
        SEND_ID=1
        ;;
    *.xml)
        TYPE=$3
        ;;
    ORIG | TERM)
        ORIGTERM=$3
        ;;
    *)
        echo "Unexpected parameter 3 - quitting"
        exit
        ;;
  esac
fi
C4=`echo $TESTNUM | cut -c1-4`
if
  test "$C4" = "mpnb" -o "$C4" = "rest"
then
  PROT=HTTP
else
  PROT=SIP
fi
case $PROT in
  HTTP)
    case $SPECIAL in
      sip) URI=sip
        ;;
      priv) URI=priv
        ;;
      mailto) URI=mailto
        ;;
      tel) URI=tel
        ;;
      *) 
        ;;
    esac
    set_ip_ports $LABCONFIGF $EDGE $USER TERM
    RC=$?
    if
      test $RC -eq 0
    then
      LOGFILE=/tmp/$$.curlfile
      RC=$(send_curl_to_edge $DSTIP $C4 $ANUM $BNUM URI:$URI PAI:$PAI)
    fi
    if
      test $RC -ne 0
    then
      echo "$RC - http call failed"
    fi
    ;;
  SIP)

    #CHECK IF IT WAS PASSED IN
    if
      test "$TYPE" = ""
    then
      TYPE=$(set_xmlfile $TESTNUM PAI:$PAI $SPECIAL)
    fi
    SIPPLOG=/tmp/$$.output
    if 
      test $SEND_ID -eq 1
    then
      if
        test "$C4" = "stir"
      then
        AS4XML=`echo $TESTNUM | cut -f1 -d"-"`
        ASEXISTS=`grep -c ^$AS4XML: $DIR/911.data`
        if
          test $ASEXISTS -eq 1
        then
          ASTEST=`grep ^$AS4XML: $DIR/911.data`
          ASPAI=`echo $ASTEST | cut -f4 -d":"`
          ASSPECIAL=`echo $ASTEST | cut -f5 -d":"`
          echo PAI:$ASPAI $ASSPECIAL $AS4XML
          OTYPE=$(set_xmlfile $AS4XML PAI:$ASPAI $ASSPECIAL)
        else
          OTYPE=verstat_passed.xml
        fi
      else
        OTYPE=verstat_passed.xml
      fi
      echo OTYPE:$OTYPE
      echo TYPE:$TYPE
      #BUILD the IDENTITY HEADER
      RC=$(sendINV_to_delegator $EDGE $USER ORIG $OTYPE $SIPPLOG)
      if
        test $RC -eq 0
      then
        LOGFILE=$(get_sipp_log $SIPPLOG)
        mv $LOGFILE $DIR/logs/$TESTNUM-AS.log
        XMLF=${TESTNUM}_withID.xml
        echo $XMLF
        cp $DIR/xml/$TYPE $DIR/xml/$XMLF
        RC=$(build_term_xmlfile $DIR/logs/$TESTNUM-AS.log $DIR/xml/$XMLF)
        if
          test $RC -eq 0
        then
          #CALL DELEGATOR WITH IDENTITY HEADER
          RC=$(sendINV_to_delegator $EDGE $USER TERM $XMLF $SIPPLOG)
          if
            test $RC -eq 0
          then
            LOGFILE=$(get_sipp_log $SIPPLOG)
          else
            echo "$RC - terminating delegator call failed"
          fi
        else
          echo "$RC - originating delegator call failed"
        fi
      else
        echo "$RC - originating delegator call failed"
      fi
    else
      echo $TYPE
      RC=$(sendINV_to_delegator $EDGE $USER $ORIGTERM $TYPE $SIPPLOG)
      if
        test $RC -eq 0
      then
        LOGFILE=$(get_sipp_log $SIPPLOG)
      else
        echo "$RC - terminating delegator call failed"
      fi
    fi
    ;;
  *)
    RC=5
    echo "$RC - unexpected protocol request"
    ;;
esac
if
  test $RC -eq 0
then
  mv $LOGFILE $DIR/logs/$TESTNUM.log
  cat $DIR/logs/$TESTNUM.log
  nohup get_tdr.sh $TESTNUM use_log 1> /dev/null 2>&1 &
fi
rm -f /tmp/$$.*
rm -f $DIR/xml/hih_$$_temp.xml
