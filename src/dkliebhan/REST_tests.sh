echo "TEST1 - Continue" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+16303060003</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST2 - Reject" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16304147970</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST3 - forward UserRule" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16302101979</calledParticipant><callingParticipant>tel:+14044559111</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST4 - forward SpamRule" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+19804858071</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST5 - display Scam Likely" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST6 - display Telemarketer" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+11212121213</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST7 - Telemarketer but not subscribed" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16302101982</calledParticipant><callingParticipant>tel:+11212121213</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo "TEST8 - scamoff - release" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551547</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST9 - Continue - SIP numbers" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>sip:+16304147970@awslab.com</calledParticipant><callingParticipant>sip:+16303060003@awslab.com</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST10 - 404 code 10" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16314147970</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST11 - 404 code 20" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551543</calledParticipant><callingParticipant>tel:+11212121213</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST12 - 404 code 30" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147971</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST13 - 404 code 40" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147973</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST14 - 400 code 50" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16825825747</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST15 - 405 GET to callScreening "
curl -i -X GET -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening
echo
echo "TEST16 - json" 
curl -i -X POST -H "Content-Type: application/json" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'{ test: "badrobot" }'
echo
echo "TEST17 - 400 invalid resource" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v2/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+16303060003</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST18 - 400 missing Parameter " 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST19 - 400 invalid flavor" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+16303060003</callingParticipant><addressDirection>Called</addressDirection><flavors><dfk/></flavors></tpc:callEventNotification>'
echo
echo "TEST20 - 403 bad server root" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/rstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+16303060003</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST23 - no display Scam Likely" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551549</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST24 - display Scam Likely" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>priv:+11212121212</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST25 - Reject" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>priv:+16304147970</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST26 - set PI indicator in TDR" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>priv:+11212121213</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST27 - display Telemarketer" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+1121212121398765</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST28 - display Scam Likely" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/callScreening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:callEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:callScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16304147970</calledParticipant><callingParticipant>tel:+1121212121298765</callingParticipant><addressDirection>Called</addressDirection><flavors><pnb/></flavors></tpc:callEventNotification>'
echo
echo "TEST29 - voicemail number from carrier default"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>tel:+16303060030</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo "TEST30 - no VM in Payload"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551547</calledParticipant><callingParticipant>tel:+11212121212</callingParticipant><addressDirection>Called</addressDirection><serviceKey>SMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST31 - email"
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>mailto:kliebhan@gmail.com</callingParticipant><addressDirection>Called</addressDirection><serviceKey>MMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
echo
echo "TEST123 - valid healthCheck - no headers" 
curl -i -H "Accept:" -X GET http://172.22.43.180:8080/firstorion.com/callScreening/v1/healthCheck
echo
#echo "TEST19b - valid healthCheck - headers" 
#curl -i -X GET -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/healthCheck
#echo
echo "TEST124 - 405 POST to healthCheck" 
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/callScreening/v1/healthCheck
curl -i -X POST -H "Content-Type: application/xml" -H "Accept: application/xml" http://172.22.43.180:8080/firstorion.com/msgScreening/v1/screening -d'<?xml version="1.0" encoding="UTF-8"?><tpc:msgEventNotification xmlns:tpc="urn:oma:xml:rest:netapi:msgScreening:1"><callSessionIdentifier>1234567890-123-12-1</callSessionIdentifier><calledParticipant>tel:+16307551540</calledParticipant><callingParticipant>mailto:kliebhan@gmail.com</callingParticipant><addressDirection>Called</addressDirection><serviceKey>MMS</serviceKey><flavors><pnb/></flavors></tpc:msgEventNotification>'
