P1=$1
if
  test "$P1" = ""
then
  echo "WARN: not sending IDENTITY header"
  sleep 10
fi
for TEST in `grep ^orch tests.data | cut -f1 -d":"`
do
  ./gen_chk.sh $TEST $P1
done
