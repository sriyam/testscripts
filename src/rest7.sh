DIR=$HOME
case $1 in
  rest)
      XML=vrest
      #XML=vrest7
      ;;
  mpnb)
      XML=mrest
      #XML=mrest7
     ;;
    *) echo 9999
     exit
     ;;
esac
A=$2
B=$3
case $4 in
  "" | tel)
    URI1=tel:+
    URI2=tel:+
    ;;
  sip)
    URI1=sip:+
    URI2=sip:+
    ;;
  priv)
    URI1=priv:+
    URI2=tel:+
    ;;
  mailto)
    URI1=mailto:
    URI2=tel:+
    ;;
esac
CHG=$5
DT=`date +%s`
cat $DIR/xml/${XML}.curl | \
  sed -e "s/DT/${DT}/" | \
  sed -e "s/AAAAAA/${A}/" | \
  sed -e "s/BBBBBB/${B}/" | \
  sed -e "s/URI1/${URI1}/g" | \
  sed -e "s/URI2/${URI2}/g" > /tmp/$$.curl
if
  test "$CHG" != ""
then 
  P1=`echo $5 | cut -f1 -d"-"`
  P2=`echo $5 | cut -f2 -d"-"`
  cat /tmp/$$.curl | sed -e "s/$P1/$P2/" > /tmp/$$.log
  cp /tmp/$$.log /tmp/$$.curl
else
  cp /tmp/$$.curl /tmp/$$.log
fi
chmod 755 /tmp/$$.curl
/tmp/$$.curl >> /tmp/$$.log
rm /tmp/$$.curl
echo $$
