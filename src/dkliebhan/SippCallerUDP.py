#!/usr/bin/env python
import argparse
import datetime
import glob
import os
import shutil
import socket
import stat
import subprocess
import sys
import urllib2


def main():
    if (len(sys.argv) > 1) and (sys.argv[1] == '--test'):
        tests()
    else:
        call = SippCaller()
        call.call()


def tests():
    pass


class SippCaller(object):
    _authinfo = None
    _urllib_timeout = 2
    _srcip = None
    _dstport = None  # default is set in the getter
    _pai = None
    _tlsdir = None
    _label = None
    _dstip = None
    xml = None
    a_party = None
    b_party = None
    ruri = None
    notls = False
    outfile = sys.stdout
    files = []
    sippargs = []

    SBCLIST = {"SBIATL20": "208.54.0.53",
               "SBICHR20": "208.54.7.68",
               "SBICHR21": "208.54.7.69",
               "SBICHR22": "208.54.6.166",
               "SBIDAL20": "208.54.8.113",
               "SBIDAL21": "208.54.8.112",
               "SBIDAL22": "208.54.48.42",
               "SBIIRV20": "208.54.10.91",
               "SBIIRV21": "208.54.10.93",
               "SBIIRV22": "208.54.6.91",
               "SBIPOL20": "208.54.47.203",
               "SBIPOL21": "208.54.47.204",
               "SBIPOL01": "208.54.16.157",
               "SBIELG20": "208.54.9.108",
               "SBIELG21": "208.54.9.109",
               "SBIELG22": "208.54.48.164",
               "SBIMAN20": "208.54.11.47",
               "SBISNQ20": "208.54.13.46",
               "SBISYO20": "208.54.71.212",
               "SBISYO21": "208.54.71.213",
               "SBIPHI20": "208.54.12.77",
               "SBIPHI21": "208.54.12.78",
               "SBIARO20": "208.54.34.149",
               "SBIARO21": "208.54.27.150",
               "SBIDET20": "208.54.92.21",
               "SBIDET21": "208.54.92.150",
               "DIDLOGIC": "sip.didlogic.net",
               "sip.didlogic.net": "sip.didlogic.net",
               "LAB_SBC": "66.94.14.219"
               }

    @property
    def dstip(self):
        return self._dstip

    @dstip.setter
    def dstip(self, dstip):
        if dstip in self.SBCLIST:
            self._dstip = self.SBCLIST[dstip]
        else:
            self._dstip = dstip

    @property
    def pai(self):
        if self._pai is None:
            return self.a_party
        else:
            return self._pai

    @pai.setter
    def pai(self, pai):
        if pai is not None:
            self._pai = pai

    @property
    def label(self):
        if self._label is None:
            return 'test_artifacts'
        else:
            return self._label

    @label.setter
    def label(self, label):
        if label is not None:
            self._label = label

    @property
    def tlsdir(self):
        if self._tlsdir is None:
            return '/tls'
        else:
            return self._tlsdir

    @tlsdir.setter
    def tlsdir(self, tlsdir):
        if tlsdir is not None:
            self._tlsdir = tlsdir

    @property
    def authinfo(self):
        if self._authinfo is None:
            return ''
        else:
            return self._authinfo

    @authinfo.setter
    def authinfo(self, authinfo):
        if authinfo is not None:
            self._authinfo = authinfo

    @property
    def dstport(self):
        if self._dstport is None:
            return 5061
        else:
            return self._dstport

    @dstport.setter
    def dstport(self, dstport):
        if dstport is not None:
            self._dstport = dstport

    @property
    def srcip(self):
        if self._srcip is None:
            url = 'http://169.254.169.254/latest/meta-data/local-ipv4/'
            self._srcip = urllib2.urlopen(
                url, timeout=self._urllib_timeout).read()
        return self._srcip

    @srcip.setter
    def srcip(self, srcip=None):
        if srcip is not None:
            self._srcip = srcip

    def __init__(self, args=None):
        self.parse_args(args)

    def parse_args(self, args=None):
        parser = argparse.ArgumentParser(
            description='Initiate a call using sipp')
        parser.add_argument(
            '-a', '--a_party', type=str, help='A Party number to use', required=True)
        parser.add_argument(
            '--pai', type=str, help='P-Asserted-Identity to use', required=False)
        parser.add_argument(
            '-b', '--b_party', type=str, help='B Party number to use', required=True)
        parser.add_argument(
            '-s', '--srcip', type=str, help='The source IP - defaults to the elastic IP assigned to the instance', required=False)
        parser.add_argument(
            '-d', '--dstip', type=str, help='The destination IP - Remote SBC - Must be specified', required=True)
        parser.add_argument(
            '--dstport', type=str, help='The destination port - Remote SBC - defaults to %s' % self.dstport, required=False)
        parser.add_argument(
            '-x', '--xml', type=str, help='Path to the XML file to  use', required=True)
        parser.add_argument(
            '-r', '--ruri', type=str, help='R-URI number to use', required=False)
        parser.add_argument(
            '--tlsdir', type=str, help='TLS Config directory (defaults to %s) to use' % self.tlsdir, required=False)
        parser.add_argument(
            '--notls', dest='notls', action='store_true', help='Suppress TLS', required=False)
        parser.set_defaults(notls=self.notls)
        parser.add_argument(
            '-l', '--label', type=str, help='Label for test (defaults to %s) - test artifacts are saved in this directory' % self.label, required=False)
        parser.add_argument('-f', '--file', type=str, action='append',
                            help='Extra files to copy (full path expected)', required=False)
        parser.add_argument('--sippargs', type=str, action='append',
                            help='Extra arguments to pass to sipp (ex: -mi 0.0.0.0:6001)', required=False)
        parser.add_argument(
            '--authinfo', type=str, help=('Authentication information for profiles that support it.'
                                          'Format: "[authentication username=theUsername password=thePassword]"'), required=False)

        if args is None:
            args = vars(parser.parse_args(sys.argv[1:]))
        else:
            args = vars(parser.parse_args(args))

        self.srcip = args['srcip']
        self.dstip = args['dstip']
        self.xml = args['xml']
        self.a_party = args['a_party']
        self.b_party = args['b_party']
        self.pai = args['pai']
        self.ruri = args['ruri']
        self.dstport = args['dstport']
        self.tlsdir = args['tlsdir']
        self.label = args['label']
        self.files = args['file'] if args['file'] is not None else []
        self.sippargs = args['sippargs'] if args['sippargs'] is not None else []
        self.authinfo = args['authinfo']
        self.notls = args['notls']

    def call(self):
        xml = open(self.xml).read()
        # TODO: this can collide, but microsecond collision chance is low and
        # our volume is low
        timestamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S.%f")

        # trailing seperator is important
        outdir = os.getcwd() + os.path.sep + self.label + \
            '_' + timestamp + os.path.sep
        os.mkdir(outdir)

        xmlFilename = self.label + '.xml'
        open(outdir + xmlFilename, 'w').write(xml)

        for file_ in self.files:
            shutil.copy(file_, outdir)

        dataFilename = self.label + "_data.csv"

        config = {"dataFilename": dataFilename,
                  "srcport": SippCaller.get_open_port(),
                  "xmlFilename": xmlFilename}
        config.update(self.getConfig())

        csv = 'SEQUENTIAL\n%(srcip)s;%(b_party)s;%(a_party)s;%(pai)s;%(ruri)s;%(label)s;%(authinfo)s' % config

        dataFile = open(outdir + dataFilename, 'w')
        dataFile.write(csv)
        dataFile.write('\n')
        dataFile.close()

        command = ['sipp',
                   '-t', 'u1' if self.notls else 'l1',
                   '-m', '1',
                   '-trace_msg',
                   '' if self.notls else '-tls_cert', '' if self.notls else '%(tlsdir)s/tst-sipp1.fosrvt.pem' % config,
                   '' if self.notls else '-tls_key', '' if self.notls else '%(tlsdir)s/tst-sipp.fosrvt.com.key' % config,
                   "%(dstip)s:%(dstport)s" % config,
                   "-sf", "%(xmlFilename)s" % config,
                   "-inf", "%(dataFilename)s" % config,
                   "-p", "%(srcport)s" % config
                   ]
        while '' in command:
            command.remove('')

        if [x for x in config['sippargs'] if x != ''] != []:
            command = command + config['sippargs']

        retryFilename = outdir + 'retry.sh'
        open(retryFilename, 'w').write(
            '#!/bin/sh\n' + subprocess.list2cmdline(command) + '\n')
        retryStats = os.stat(retryFilename)
        os.chmod(retryFilename, stat.S_IEXEC | retryStats.st_mode)

        try:
            process = subprocess.Popen(
                command, bufsize=1, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=outdir)
            output = process.stdout
            while True:
                outbuf = output.readline()
                if not outbuf:
                    break
                self.outfile.write(outbuf)
                self.outfile.flush()
        except subprocess.CalledProcessError, e:
            self.output = e.output
            self.response_code = e.returncode
            self.outfile.write(self.output)
            self.outfile.flush()

        self.printLastLog(outdir)

        self.outfile.write("Output to %s\n" % (self.label + '_' + timestamp,))

    def printLastLog(self, outdir):
        lastLog = max(
            glob.iglob(outdir + os.path.sep + '*.log'), key=os.path.getctime)
        if lastLog:
            self.outfile.write(open(lastLog).read())
            self.outfile.flush()

    @staticmethod
    def get_open_port():
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(("", 0))
        s.listen(1)
        port = s.getsockname()[1]
        s.close()
        return port

    def getConfig(self):
        config = {"srcip": self.srcip,
                  "dstip": self.dstip,
                  "dstport": self.dstport,
                  "xml": self.xml,
                  "a_party": self.a_party,
                  "b_party": self.b_party,
                  "pai": self.pai,
                  "ruri": self.ruri,
                  "tlsdir": self.tlsdir,
                  "label": self.label,
                  "sippargs": ' '.join(self.sippargs).split(' '),
                  "authinfo": self.authinfo
                  }
        return config


if __name__ == '__main__':
    main()
