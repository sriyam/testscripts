function get_tdr_by_date()
{
  local DT=$1
  local CALLID=$2
  local USER=`echo $HOME | cut -f3 -d"/"`
  local PARM1
  local PARM2
  local FND
  case $CALLID in
     HTTP:*)
       PARM1=`echo $CALLID | cut -f2 -d":"`
       PARM2=`echo $CALLID | cut -f3 -d":"`
       ssh ${USER}@${LAB} "zgrep $PARM1 /opt/titan/tdr/SYSTEM/local/${DT}*.gz | grep $PARM2 | grep HTTP" 1> $OUTFILE 2>/tmp/$$.errorout
       ;;
     *)
       ssh ${USER}@${LAB} "zgrep $CALLID /opt/titan/tdr/SYSTEM/local/${DT}*.gz" 1> $OUTFILE 2>/tmp/$$.errorout
       ;;
  esac
  if
    test -r $OUTFILE
  then
    FND=`cat $OUTFILE | wc -l`
  else
    FND=0
  fi
  return $FND
}

function get_tdr()
{
  #Yes - sometimes the server stamps are off enough that the TDR occurs before the call timestamp on the source server so DT3
  local DT1=$1
  local DT2=$2
  local DT3=$3
  local CALLID=$4
  get_tdr_by_date $DT1 $CALLID
  FND=$?
  if
    test $FND -eq 0
  then
    get_tdr_by_date $DT2 $CALLID
    FND=$?
    if
      test $FND -eq 0
    then
      get_tdr_by_date $DT3 $CALLID
      FND=$?
    fi
  fi
  return $FND
}

DIR=$HOME
TEST=$1
DTPARM=$3
VERBOSE=$4
LOG=$DIR/logs/$TEST.log
TDRF=$DIR/logs/$TEST.tdr
rm -f $TDRF
if
  test ! -r $LOG
then
  echo "LOG $LOG not found"
  echo "LOG $LOG not found" > $TDRF
  exit
fi
C4=`echo $TEST | cut -c1-4`
case $C4 in
   mpnb|rest)
      CDPN=`grep curl $LOG | cut -f2 -d'+' | cut -f1 -d'<'`
      CGPN=`grep curl $LOG | cut -f3 -d'+' | cut -f1 -d'<'`
      CALLID=HTTP:$CDPN:$CGPN
      ;;
   *)
      CALLID=`cat $LOG | grep -m1 Call-ID | cut -f2 -d" " | cut -f1 -d"@"`
      ;;
esac
case $2 in
  use_log)
      case $CALLID in
         HTTP:*)
            LAB=`grep curl $LOG | cut -f4 -d":" | cut -c3-`
            ;;
         *)
            LAB=`grep -m 1 ^INVITE $LOG | cut -f2 -d"@" | cut -f1 -d":"`
            ;;
      esac
      if
        test "$DTPARM" = ""
      then
        DTPARM="use_log"
      fi
      ;;
   *)
      LAB=$2
      ;;
esac
if
  test "$LAB" = ""
then
  echo "LAB to check is not specified"
  echo "LAB to check is not specified" > $TDRF
  exit
fi
case $DTPARM in
  use_log)
      DT=`stat -c '%Y' $LOG`
      ;;
  "")
      DT=`date +%s`
      ;;
  *)
      DT=${3}
      ;;
esac
DTNOW=`date +%s`
DTDIFF=`expr $DTNOW \- $DT`
if
  test $DTDIFF -lt 120
then
  sleep 120
fi
DT1=`date -d@${DT} +%Y%m%d%H%M | cut -c1-11`
DT1_PLUS10=`expr $DT \+ 600`
DT1_MINUS10=`expr $DT \- 600`
DT2=`date -d@${DT1_PLUS10} +%Y%m%d%H%M | cut -c1-11`
DT3=`date -d@${DT1_MINUS10} +%Y%m%d%H%M | cut -c1-11`
OUTFILE=/tmp/tdr.$TEST.$CALLID
get_tdr $DT1 $DT2 $DT3 $CALLID
FND=$?
if
  test $FND -eq 0
then
  echo "TDR:No TDR FOUND for $TEST" > $TDRF
else
  if
    test "$VERBOSE" = "v"
  then
    cat $OUTFILE
  fi
  mv $OUTFILE $TDRF
  #while
  #  read TDR
  #do
  #  #echo $TDR
  #  echo $TDR >> $TDRF
  #  #echo $JUSTDATA >> $TDRF
  #done < $OUTFILE
fi
rm /tmp/$$.*
