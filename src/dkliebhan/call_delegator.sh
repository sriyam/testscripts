DIR=$HOME
SRCIP=$1
SRCPORT=$2
DSTIP=$3
DSTPORT=$4
ANUM=$5
BNUM=$6
TYPEF=$7
PAI=$8
LIMITER=$9
#INIT VARIABLES
DOVS=0
if
  test "$1" = "" -o "$2" = "" -o "$3" = "" -o "$4" = "" -o "$5" = "" -o "$6" = "" -o "$7" = "" 
then
  echo "failed 7 parameters are required.  Missing at least 1"
  echo "USAGE: call_delegator SourceIP SourcePort DestIP DestPort Anumber Bnumber TypeFile.xml PAI LIMITER"
  exit
fi
#BEGIN
if
  test "$TYPEF" = "sti-as.xml" 
then
  if
    test "$LIMITER" != "ASONLY"
  then
    DOVS=1
  fi
  AS_SRCPORT=9099
  AS_DSTPORT=7980
  VS_SRCPORT=$2
  VS_DSTPORT=$4
else
  AS_SRCPORT=$2
  AS_DSTPORT=$4
  VS_SRCPORT=$2
  VS_DSTPORT=$4
fi
if
  test "$PAI" = "NA"
then
  $DIR/SippCaller.py -a $ANUM -b $BNUM -s $SRCIP --srcport $AS_SRCPORT -d $DSTIP --dstport $AS_DSTPORT --notls -x $DIR/xml/$TYPEF 1>/tmp/$$.output
else
  $DIR/SippCaller.py -a $ANUM --pai $PAI -b $BNUM -s $SRCIP --srcport $AS_SRCPORT -d $DSTIP --dstport $AS_DSTPORT --notls -x $DIR/xml/$TYPEF 1>/tmp/$$.output
fi
if
  test "$DOVS" = "1"
then
  $DIR/bld2.sh /tmp/$$.output 
  if
    test -r xml/sti-vs.xml
  then
  $DIR/SippCaller.py -a $ANUM -b $BNUM -s $SRCIP --srcport $VS_SRCPORT -d $DSTIP --dstport $VS_DSTPORT --notls -x $DIR/xml/sti-vs.xml 1>/tmp/$$.output2 2>&1
  else
    echo "could not run VS as no sti-vs.xml file generated" >> /tmp/$$.output
    DOVS=0
  fi
fi
#retrive logs
OUTDIR=`tail -1 /tmp/$$.output | cut -f3 -d" "`
if
  test -r $OUTDIR
then
  cp $OUTDIR/*.log /tmp/$$.log
  rm -rf $OUTDIR
  if
    test "$DOVS" = "1"
  then
    OUTDIR=`tail -1 /tmp/$$.output2 | cut -f3 -d" "`
    if
      test -r $OUTDIR
    then
      cat $OUTDIR/*.log >> /tmp/$$.log
      rm -rf $OUTDIR
    fi
  fi
else
  mv /tmp/$$.output /tmp/$$.log
fi
echo $$
