#!/bin/bash

change_line () {
   CHG=$1
   CHGWHAT=$2
   TOWHAT=$3
   FILE=$4
   TMPFILE=/tmp/$$.chgline
   case $CHG in
     repl)
       MODL=$TOWHAT
       ;;
     chgv)
       cat $FILE | sed -e "s/${CHGWHAT}/${TOWHAT}/" > $TMPFILE
       MODL=""
       mv $TMPFILE $FILE
       ;;
     chgg)
       cat $FILE | sed -e "s/${CHGWHAT}/${TOWHAT}/g" > $TMPFILE
       MODL=""
       mv $TMPFILE $FILE
       ;;
     del)
       grep -v $CHGWHAT $FILE > $TMPFILE
       MODL=""
       mv $TMPFILE $FILE
       ;;
   esac
   if
     test "$MODL" != ""
   then
     MODN=`grep -m 1 -n "${CHGWHAT}" $FILE | cut -f1 -d":"`
     if
       test "$MODN" != ""
     then
       N=`expr $MODN \- 1`
       head -$N $FILE > $TMPFILE
       echo $MODL >> $TMPFILE
       N=`expr $MODN \+ 1`
       tail -n +$N $FILE >> $TMPFILE
       mv $TMPFILE $FILE
     fi
   fi
}

DIR=$HOME
TESTDATA=`grep ^${1}: $DIR/tests.data`
if
  test "$TESTDATA" = ""
then
  echo "need valid tdrname to save"
  exit
fi
TESTNUM=`echo $TESTDATA | cut -f1 -d":"`
C4=`echo $TESTNUM | cut -c1-4`
TDRF="$DIR/logs/${TESTNUM}.tdr"
if
  test ! -f $TDRF
then
  echo "1:$TESTNUM:no response tdr file to compare against template"
  exit
fi
USER=`echo $HOME | cut -f3 -d"/"`
RESPLOG=/tmp/$$.RESPLOG
USELOG=/tmp/$$.logname
CHGTEMPL=$2 #TEMPLATES are in 7.14
TEMPLATE="./diffdir/${C4}/${TESTNUM}.tdr"
TEMPLNAME=/tmp/$$.template
if
  test ! -f $TEMPLATE
then
  echo "1:$TEMPLATE:no tdr file in diffdir/$C4"
  exit
fi
cp $TEMPLATE $TEMPLNAME
#
# FIX TEMPLATES TO NEW STYLE
#
if
  test "$CHGTEMPL" = "y"
then
  for WORKER in `echo FONES CALLER_YD FINGER_PRINT STIR`
  do
    if
      test "$WORKER" = "FONES"
    then
      #Fix CALLID in every worker and add final |
      ONEtoTHREE=`grep $WORKER $TEMPLNAME | cut -f1-5 -d"|"`
      FIVEon=`grep $WORKER $TEMPLNAME | cut -f7- -d"|"`
      NEWREC="${ONEtoTHREE}||${FIVEon}"
    else
      #Fix CALLID in every worker
      ONEtoTHREE=`grep $WORKER $TEMPLNAME | cut -f1-3 -d"|"`
      FIVEon=`grep $WORKER $TEMPLNAME | cut -f5- -d"|"`
      NEWREC="${ONEtoTHREE}||${FIVEon}"
    fi
    change_line repl "${WORKER}" "${NEWREC}" $TEMPLNAME
  done
  #REMOVE teminanting NONE
  #FONESDIF=`grep FONES $TEMPLNAME | grep "|NONE$"`
  #if
  #  test "$FONESDIF" != ""
  #then
  #  cat $TEMPLNAME | sed -e "s/|NONE$//" > /tmp/$$.newtempl
  #  TEMPLNAME=/tmp/$$.newtempl
  #fi
  ##REMOVE TRANSIT CATEGORY
  #FONESDIF=`grep FONES $TEMPLNAME | grep "|32766|"`
  #if
  #  test "$FONESDIF" != ""
  #then
  #  cat $TEMPLNAME | sed -e "s/|32766|/||/" > /tmp/$$.newtempl2
  #  TEMPLNAME=/tmp/$$.newtempl2
  #fi
  #SS=`grep -c SHAKEN $TEMPLNAME`
  #if
  #  test "$SS" = "1"
  #then
  #   #REMOVE BPV value
  #   NEWSS=`grep SHAKEN $TEMPLNAME | sed -e "s/|0|0$//"`
  #   change_line repl 'SHAKEN' "${NEWSS}" $TEMPLNAME
  #fi
fi
case $C4 in
  rest | mpnb)
    # PNB and MPNB
    #STRESPL=`grep -n -m1 "^HTTP" ${USELOG} | cut -f1 -d":"`
    #ENDRESPL=`cat ${USELOG} | wc -l`
    ;;
  *)
    # SIP ************************************************
    DELcnt=`grep -c DELEGATOR $TDRF`
    if
      test $DELcnt -ne 1
    then
      echo "1:$TESTNUM No Delegator record in TDR file as expected"
      exit
    fi
    EDGE=`grep DELEGATOR $TDRF | cut -f3 -d"|"`
    CALLID=`grep DELEGATOR $TDRF | cut -f7 -d"|"`
    TIMESTAMP=`grep DELEGATOR $TDRF | cut -f8 -d"|"`
    CONTEXT=`grep DELEGATOR $TDRF | cut -f9 -d"|"`
    SRCIP=`grep DELEGATOR $TDRF | cut -f11 -d"|"`
    DSTIP=`grep DELEGATOR $TDRF | cut -f12 -d"|"`
    PORT=`grep DELEGATOR $TDRF | cut -f13 -d"|"`
    #STRESPL=`grep -n -m1 "302 Moved Temporarily" ${USELOG} | cut -f1 -d":"`
    #if
    #  test "$STRESPL" = ""
    #then 
    #  STRESPL=`grep -n -m1 "487 Request Terminated" ${USELOG} | cut -f1 -d":"`
    #fi
    #ENDRESPL=`grep -n -m1 "Content-Length: 0" ${USELOG} | cut -f1 -d":"`
    #;;
esac
LEN=`echo $TIMESTAMP | wc -c`
if
  test $LEN -lt 14
then
  FIXSTAMP=TIMESTAMP
else
  FIXSTAMP=TIMESTAMP
fi
#if
#  test "${STRESPL}" = "" -o "${ENDRESPL}" = ""
#then
#  echo "1:$TESTNUM:Cannot find response payload to save"
#  exit
#else
#  tail -n +${STRESPL} $USELOG | head -n $(($ENDRESPL - $STRESPL + 1)) > $RESPLOG
case $C4 in
    rest | mpnb)
      change_line repl '^Date:' 'Date: XXXXXX' $RESPLOG
      change_line chgv '>123456-.*<' '>123456<' $RESPLOG
      ;;
    *)
      cut -f2- -d"|" $TDRF | \
         sed -e "s/$EDGE/EDGE/" | \
         sed -e "s/$CALLID/CALLID/g" | \
         sed -e "s/$TIMESTAMP|$CONTEXT/$FIXSTAMP|CONTEXT/" | \
         sed -e "s/$CONTEXT/CONTEXT/" | \
         sed -e "s/$SRCIP/SRCIP/" | \
         sed -e "s/$DSTIP/DSTIP/" | \
         sed -e "s/|$PORT|/|PORT|/" > $RESPLOG
        #SS=`grep -c SHAKEN $RESPLOG`
        #if
        #  test "$SS" = "1"
        #then
        #   NEWSS=`grep SHAKEN $RESPLOG | sed -e "s/CALLID-5/CALLID-7/g" | sed -e "s/0|200||/200|200|Ok|/g" | sed -e "s/A||0|1/A|0|0|0/" |cut -f1-34 -d"|"`
        #   change_line repl 'SHAKEN' "${NEWSS}" $RESPLOG
        #fi
      #cat $RESPLOG
      if
        test "$CHGTEMPL" = "y"
      then
        #Remove ROUTING TDR
        change_line del 'ROUTING' "" $RESPLOG
        #Rebuild DELEGATOR TDR
        grep "DELEGATOR-SPAF" $RESPLOG > /tmp/$$.DELrec
        DELREC="DELEGATOR|`cat /tmp/$$.DELrec | cut -f2-12 -d"|"`"
        grep "DISTRIBUTOR" $RESPLOG > /tmp/$$.DELrec
        DELREC="$DELREC|`cat /tmp/$$.DELrec | cut -f7 -d"|"`"
        DELREC="$DELREC|`cat /tmp/$$.DELrec | cut -f8 -d"|" | sed -e "s/:10//" | sed -e "s/://g"`"
        grep "ORCHESTRATOR" $RESPLOG > /tmp/$$.DELrec
        DELREC="$DELREC|`cat /tmp/$$.DELrec | cut -f7 -d"|"`"
        grep "DELEGATOR-SPAF" $RESPLOG > /tmp/$$.DELrec
        DELREC="$DELREC|`cat /tmp/$$.DELrec | cut -f13-16 -d"|"`"
        change_line del 'DELEGATOR-SPAF' "" $RESPLOG
        change_line del 'DISTRIBUTOR' "" $RESPLOG
        change_line del 'ORCHESTRATOR' "" $RESPLOG
        TMPREC1=`echo $DELREC | sed -e "s/-10:.//"`
        TMPREC2=`echo $TMPREC1 | sed -e "s/:10//"`
        DELREC=`echo $TMPREC2 | sed -e "s/CONTEXT/SRCIP/g" | sed -e "s/SRCIP/CONTEXT/"`
        echo $DELREC >> $RESPLOG
      fi

      #change_line chgv ';branch=.*;' ';branch=XXXXX;' $RESPLOG
      #change_line chgv ';branch=.*-0$' ';branch=XXXXX' $RESPLOG
      #change_line chgv 'tag=.*$' 'tag=YYYYY' $RESPLOG
      #change_line chgv 'Call-ID: .*@' 'Call-ID ZZZZZ@' $RESPLOG
      #change_line chgv "$SRCIP" 'SRCIP' $RESPLOG
      #change_line chgv ":${SRCPORT}" ':SRCPORT' $RESPLOG
      #change_line chgv "=${SRCPORT}" '=SRCPORT' $RESPLOG
      #change_line chgv "$DSTIP" 'DSTIP' $RESPLOG
      #change_line chgv ":${DSTPORT}" ':DSTPORT' $RESPLOG
      #change_line chgv '^Identity: .*;' 'Identity: IIIII;' $RESPLOG
      #change_line repl '^P-Origination-Id:' 'P-Origination-Id: HHHHH' $RESPLOG
      #change_line repl '^Date:' 'Date: DDDDD' $RESPLOG
      ;;
esac
  #cat $RESPLOG
RC="0"
DIFFILE=/tmp/$$.diff_$TESTNUM
#diff $RESPLOG $TEMPLNAME > $DIFFILE
sort $RESPLOG > /tmp/$$.sort_resplog
sort $TEMPLNAME > /tmp/$$.sort_templlog
diff /tmp/$$.sort_resplog /tmp/$$.sort_templlog > $DIFFILE
DIFFCNT=`cat $DIFFILE | wc -l`
if
  test $DIFFCNT -gt 0
then
  grep "^<" $DIFFILE | cut -c3- > /tmp/$$.olddiff1
  grep "^>" $DIFFILE | cut -c3- > /tmp/$$.newdiff1
  diff /tmp/$$.olddiff1 /tmp/$$.newdiff1 > /tmp/$$.difftdr2
  DIFFCNT2=`cat /tmp/$$.difftdr2 | wc -l`
  if
    test $DIFFCNT2 -gt 0
  then
    RC="1"
  fi
fi
VERBOSE=1
if
   test "${VERBOSE}" != "" 
then
   echo "$TESTNUM <new >old"
   WC=0
   if
     test -f /tmp/$$.difftdr2
   then
     WC=`cat /tmp/$$.difftdr2 | wc -c`
   fi
   if
     test $WC -gt 0
   then
     cat /tmp/$$.difftdr2
   else
     echo "no differences"
   fi
else
   echo $RC
fi
#cat $RESPF
rm -f /tmp/$$.*
